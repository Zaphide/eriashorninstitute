== README ==
Esiran Synarch is a faction mod for Fractalsoftwork's StarSector (previously StarFarer).


== LICENCE ==
All code licenced under MIT Licence (https://opensource.org/licenses/MIT)
For sprites see Helmut or Zaphide (or Fractal Softworks for some originals)


CREDITS
Original Sprites    - Helmut, Fractal Softworks
Original Logo/Crest - Helmut

Code Samples        - Cycerin, Sundog, Dark.Revenant, MesoTronik, AxleMC131
Supporting Code     - LazyWizard (LazyLib)


== SHIPS ==
Addendum (Small Bolter Platform, combat deployable)
Anthology (Defense Fighter)
Abstract (Interceptor)
Corpus (Advanced Phase Shift Bomber)
Extract (Heavy Assault Fighter)
Synopsis (Advanced Disruption Fighter)
Translate (Fighter/Bomber)

Poet (Medium Bolter Platform, combat deployable)
Amanuensis (Freighter)
Historian (Patrol/Intercept Frigate)
Philosopher (Sniper/Support Frigate)
Sage (Drone Frigate)
Sophos (Bolter Platform Frigate)
Artist (Advanced Phase Assault Frigate)
Scholar (Advanced Disruption Frigate)

Perception (Sensor/Support Destroyer)
Inform (Courier Destroyer)
Attend (Phase Freighter, converted Buffalo)
Enlighten (Phase Destroyer, converted Medusa)
Astute (Defense Platform)

Gnostic (Heavy Assault Cruiser)
Esoteric (Phase Cruiser, converted Aurora)
Stoic (Combat Freighter)
Logos (Combat Tanker)

Polemarch (Converted Odyssey)
Strategos (Fleet Support Carrier)
Archon (Heavy Battleship)


== SKINS ==
Falcon (E)
Sundar (E)


== WEAPONS ==
PD Lance
Bolter (fighter, light, medium, siege)
Subversion Missile


== AVAILABLE HULLMODS ==
PD Drone Bay
Assault Drone Bay
Phase Assault Drone Bay
Network Pattern Etching


== GENERAL FACTION INFO ==
- A council formed after collapse in a system with very limited population + cut off by hyperspace storms
- Logisitic / Patient / Observe / Automation / Defensive

Color
 - Ships:   White (primary) / Light Yellow (secondary)
 - Weapons: Grey (primary) / Light Yellow (secondary)
 - Proj:    Yellow (primary) / White (secondary)

Ship Naming conventions
- Knowledge / Textual / Theory-of

History (Esiran Synarch)
- Loose polity formed for immediate survival during post-collapse when Eria isolated by Hyperspace storms
  - Combination of military, research, civil, corporate-leaders, initally for survival, synarchy form of government
- Focus on tech/automation/sensor
- Recently built blockade runners for Askonia Crisis
- Some collaboration with TriTach for ship design, some collaboration with Sindria for recent fleet doctrine and auxiliaries

Ranks
- Courier
- Observer

- Liason
- Tactician

- Navigator
- Commander

- Executor
- Adjutant

- Steward
- Marshal


== FACTION INTEGRATION INFO ==
Campaign Integration
- New system 'Esira'
 - Galactic North, in heavy storms area (is this possible?)
 - Heavy nebulae, blue cold star, requires strong sensors
 - Lots of cold/ice planets

Political
- Friendly 
  - TriTachyon 
  - Sindria
- Neutral
  - League
  - Indepedents
- Suspicious
 - Hegemony
- Hostile
  - Church
  - Path
  - Pirates

Illegal Goods
- Hand Weapons
- Organs


== COMBAT SPECIFICS ==
Positives
- Strong shield stats
- Strong sensor/visual stats
- Strong flux capacity

Neutal
- Medium manuaverability
- Medium/Strong flux vents

Negatives
- Poor armor stats
- Poor base top speed
- Generally low total number of weapon mounts on hulls and low ordanance pointts

Detail
- High number of built-in weapons (built-in's focus on EMP, missiles)
- Lack of customisation on some hulls
- Minimal ballistic weapon slots
- Focus on drones/automation
 - Systems with drones always regenerate
 - PD, assualt versions
 - Combat deployables (heavier, autonomous drones)
- Phase Shift hulls
- Support/defensive ship systems


== TODO ==
- Sprites
 - Sunder (E) rework
 - Archon painting

- Weapons
 - Trojan AI AoE EMP un-aimed torpedo
  - Add to Corpus bomber
 - Medium bolter (that is not built-in only)

- Systems
 - Maybe a FortressShield-like ship system for some ships?

- General
 - Add more ship names

- Campaign
 - Additional system for fronteir expansion? Or maybe just use closest non-occupied proc-genned one?

== BUILD REQUIRMENTS ==
 - JDK 1.7
 - StarFarer API
 - lwjgl
 - lwjgl_util
 - log4j

 - lazylib
 - nexerilin