package data.missions.esi_test2;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets so we can add ships and fighter wings to them.
		// In this scenario, the fleets are attacking each other, but
		// in other scenarios, a fleet may be defending or trying to escape
		api.initFleet(FleetSide.ENEMY, "ESI", FleetGoal.ATTACK, false);
		api.initFleet(FleetSide.PLAYER, "ANY", FleetGoal.ATTACK, true);

		// Set a small blurb for each fleet that shows up on the mission detail and
		// mission results screens to identify each side.
		api.setFleetTagline(FleetSide.PLAYER, "ENEMY-TEST");
		api.setFleetTagline(FleetSide.ENEMY, "ESI-TEST");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("Test");


		api.addToFleet(FleetSide.PLAYER, "mule_d_pirates_Smuggler", FleetMemberType.SHIP, "Cannon Fodder", false);
		api.addToFleet(FleetSide.PLAYER, "mule_d_pirates_Smuggler", FleetMemberType.SHIP, "Cannon Fodder", false);
		api.addToFleet(FleetSide.PLAYER, "mule_d_pirates_Smuggler", FleetMemberType.SHIP, "Cannon Fodder", false);
		api.addToFleet(FleetSide.PLAYER, "kite_Raider", FleetMemberType.SHIP, "Cannon Fodder1", false);
		api.addToFleet(FleetSide.PLAYER, "lasher_d_CS", FleetMemberType.SHIP, "Cannon Fodder2", false);
		api.addToFleet(FleetSide.PLAYER, "falcon_d_CS", FleetMemberType.SHIP, "Cannon Fodder3", false);
		api.addToFleet(FleetSide.PLAYER, "falcon_d_CS", FleetMemberType.SHIP, "Cannon Fodder3a", false);
		api.addToFleet(FleetSide.PLAYER, "aurora_Balanced", FleetMemberType.SHIP, "Cannon Fodder10", false);
		api.addToFleet(FleetSide.PLAYER, "odyssey_Balanced", FleetMemberType.SHIP, "Cannon Fodder10", false);
		api.addToFleet(FleetSide.PLAYER, "aurora_Balanced", FleetMemberType.SHIP, "Cannon Fodder10", false);
		api.addToFleet(FleetSide.PLAYER, "odyssey_Balanced", FleetMemberType.SHIP, "Cannon Fodder11", true);
		
		// Set up the enemy's fleet.  Variant names come from the
		// files in data/variants and data/variants/fighters
		api.addToFleet(FleetSide.ENEMY, "esi_archon_standard", FleetMemberType.SHIP, "Test Ship", true);
		api.addToFleet(FleetSide.ENEMY, "esi_polemarch_standard", FleetMemberType.SHIP, "Test Ship", false);
		api.addToFleet(FleetSide.ENEMY, "esi_strategos_standard", FleetMemberType.SHIP, "Test Ship5", false);

		api.addToFleet(FleetSide.ENEMY, "esi_falcon_standard", FleetMemberType.SHIP, "Test Ship6", false);
		api.addToFleet(FleetSide.ENEMY, "esi_esoteric_standard", FleetMemberType.SHIP, "Test Ship6", false);
		api.addToFleet(FleetSide.ENEMY, "esi_gnostic_standard", FleetMemberType.SHIP, "Test Ship15", false);
		api.addToFleet(FleetSide.ENEMY, "esi_stoic_standard", FleetMemberType.SHIP, "Test Ship15", false);
		api.addToFleet(FleetSide.ENEMY, "esi_logos_standard", FleetMemberType.SHIP, "Test Ship15", false);

		api.addToFleet(FleetSide.ENEMY, "esi_astute_standard", FleetMemberType.SHIP, "Test Ship16", false);
		api.addToFleet(FleetSide.ENEMY, "esi_perception_standard", FleetMemberType.SHIP, "Test Ship18", false);
		api.addToFleet(FleetSide.ENEMY, "esi_inform_standard", FleetMemberType.SHIP, "Test Ship3", false);
		api.addToFleet(FleetSide.ENEMY, "esi_attend_standard", FleetMemberType.SHIP, "Test Ship2", false);
		api.addToFleet(FleetSide.ENEMY, "esi_enlighten_standard", FleetMemberType.SHIP, "Test Ship12", false);
		api.addToFleet(FleetSide.ENEMY, "esi_sunder_standard", FleetMemberType.SHIP, "Test Ship12", false);

		api.addToFleet(FleetSide.ENEMY, "esi_artist_standard", FleetMemberType.SHIP, "Test Ship14", false);
		api.addToFleet(FleetSide.ENEMY, "esi_historian_standard", FleetMemberType.SHIP, "Test Ship17", false);
		api.addToFleet(FleetSide.ENEMY, "esi_philosopher_standard", FleetMemberType.SHIP, "Test Ship19", false);
		api.addToFleet(FleetSide.ENEMY, "esi_sage_standard", FleetMemberType.SHIP, "Test Ship31", false);
		api.addToFleet(FleetSide.ENEMY, "esi_sophos_standard", FleetMemberType.SHIP, "Test Ship39", false);
		api.addToFleet(FleetSide.ENEMY, "esi_scholar_standard", FleetMemberType.SHIP, "Test Ship39", false);
		api.addToFleet(FleetSide.ENEMY, "esi_amanuensis_standard", FleetMemberType.SHIP, "Test Ship39", false);
		
		api.defeatOnShipLoss("Cannon Fodder11");
		
		// Set up the map.
		float width = 12000f;
		float height = 12000f;
		api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);
		
		float minX = -width/2;
		float minY = -height/2;
		
		// Add an asteroid field
		api.addAsteroidField(minX, minY + height / 2, 0, 8000f,
							 20f, 70f, 100);

		api.addPlanet(0, 0, 200f, "barren", 0f, true);
		
	}

}
