package data.missions.esi_vs_random;

import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import data.missions.BaseRandomEsiMissionDefinition;

public class MissionDefinition extends BaseRandomEsiMissionDefinition{

	public void defineMission(MissionDefinitionAPI api) {

		chooseFactions("esi", null);
		super.defineMission(api);
		
	}

}
