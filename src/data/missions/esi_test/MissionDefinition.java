package data.missions.esi_test;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets so we can add ships and fighter wings to them.
		// In this scenario, the fleets are attacking each other, but
		// in other scenarios, a fleet may be defending or trying to escape
		api.initFleet(FleetSide.PLAYER, "ESI", FleetGoal.ATTACK, false);
		api.initFleet(FleetSide.ENEMY, "ENY", FleetGoal.ATTACK, true);

		// Set a small blurb for each fleet that shows up on the mission detail and
		// mission results screens to identify each side.
		api.setFleetTagline(FleetSide.PLAYER, "ESI-TEST");
		api.setFleetTagline(FleetSide.ENEMY, "ENEMY-TEST");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("Test");
		
		
		// Set up the player's fleet.  Variant names come from the
		// files in data/variants and data/variants/fighters
		api.addToFleet(FleetSide.PLAYER, "esi_archon_standard", FleetMemberType.SHIP, "Test Ship", true);
		api.addToFleet(FleetSide.PLAYER, "esi_polemarch_standard", FleetMemberType.SHIP, "Test Ship2", false);
		api.addToFleet(FleetSide.PLAYER, "esi_strategos_standard", FleetMemberType.SHIP, "Test Ship3", false);

		api.addToFleet(FleetSide.PLAYER, "esi_falcon_standard", FleetMemberType.SHIP, "Test Ship4", false);
		api.addToFleet(FleetSide.PLAYER, "esi_esoteric_standard", FleetMemberType.SHIP, "Test Ship5", false);
		api.addToFleet(FleetSide.PLAYER, "esi_gnostic_standard", FleetMemberType.SHIP, "Test Ship6", false);
		api.addToFleet(FleetSide.PLAYER, "esi_stoic_standard", FleetMemberType.SHIP, "Test Ship7", false);
		api.addToFleet(FleetSide.PLAYER, "esi_logos_standard", FleetMemberType.SHIP, "Test Ship8", false);

		api.addToFleet(FleetSide.PLAYER, "esi_astute_standard", FleetMemberType.SHIP, "Test Ship9", false);
		api.addToFleet(FleetSide.PLAYER, "esi_perception_standard", FleetMemberType.SHIP, "Test Ship10", false);
		api.addToFleet(FleetSide.PLAYER, "esi_inform_standard", FleetMemberType.SHIP, "Test Ship11", false);
		api.addToFleet(FleetSide.PLAYER, "esi_attend_standard", FleetMemberType.SHIP, "Test Ship12", false);
		api.addToFleet(FleetSide.PLAYER, "esi_enlighten_standard", FleetMemberType.SHIP, "Test Ship13", false);
		api.addToFleet(FleetSide.PLAYER, "esi_sunder_standard", FleetMemberType.SHIP, "Test Ship14", false);

		api.addToFleet(FleetSide.PLAYER, "esi_artist_standard", FleetMemberType.SHIP, "Test Ship15", false);
		api.addToFleet(FleetSide.PLAYER, "esi_historian_standard", FleetMemberType.SHIP, "Test Ship16", false);
		api.addToFleet(FleetSide.PLAYER, "esi_philosopher_standard", FleetMemberType.SHIP, "Test Ship17", false);
		api.addToFleet(FleetSide.PLAYER, "esi_sage_standard", FleetMemberType.SHIP, "Test Ship18", false);
		api.addToFleet(FleetSide.PLAYER, "esi_sophos_standard", FleetMemberType.SHIP, "Test Ship19", false);
		api.addToFleet(FleetSide.PLAYER, "esi_scholar_standard", FleetMemberType.SHIP, "Test Ship20", false);
		api.addToFleet(FleetSide.PLAYER, "esi_amanuensis_standard", FleetMemberType.SHIP, "Test Ship21", false);
		api.addToFleet(FleetSide.PLAYER, "esi_poet_standard", FleetMemberType.SHIP, "Test Ship22", false);

		// Set up the enemy fleet.
		api.addToFleet(FleetSide.ENEMY, "mule_d_pirates_Smuggler", FleetMemberType.SHIP, "Cannon Fodder14", false);
		api.addToFleet(FleetSide.ENEMY, "mule_d_pirates_Smuggler", FleetMemberType.SHIP, "Cannon Fodder15", false);
		api.addToFleet(FleetSide.ENEMY, "mule_d_pirates_Smuggler", FleetMemberType.SHIP, "Cannon Fodder16", false);
		api.addToFleet(FleetSide.ENEMY, "kite_Raider", FleetMemberType.SHIP, "Cannon Fodder1", false);
		api.addToFleet(FleetSide.ENEMY, "lasher_d_CS", FleetMemberType.SHIP, "Cannon Fodder2", false);
		api.addToFleet(FleetSide.ENEMY, "falcon_d_CS", FleetMemberType.SHIP, "Cannon Fodder3", false);
		api.addToFleet(FleetSide.ENEMY, "falcon_d_CS", FleetMemberType.SHIP, "Cannon Fodde4", false);
		api.addToFleet(FleetSide.ENEMY, "dagger_wing", FleetMemberType.FIGHTER_WING, "Cannon Fodder5", true);
		api.addToFleet(FleetSide.ENEMY, "talon_wing", FleetMemberType.FIGHTER_WING, "Cannon Fodder6", true);
		api.addToFleet(FleetSide.ENEMY, "dagger_wing", FleetMemberType.FIGHTER_WING, "Cannon Fodder7", true);
		api.addToFleet(FleetSide.ENEMY, "talon_wing", FleetMemberType.FIGHTER_WING, "Cannon Fodder8", true);
		api.addToFleet(FleetSide.ENEMY, "aurora_Balanced", FleetMemberType.SHIP, "Cannon Fodder10", false);
		api.addToFleet(FleetSide.ENEMY, "odyssey_Balanced", FleetMemberType.SHIP, "Cannon Fodder11", false);
		api.addToFleet(FleetSide.ENEMY, "aurora_Balanced", FleetMemberType.SHIP, "Cannon Fodder12", false);
		api.addToFleet(FleetSide.ENEMY, "odyssey_Balanced", FleetMemberType.SHIP, "Cannon Fodder13", false);
		
		api.defeatOnShipLoss("Test Ship");
		
		// Set up the map.
		float width = 12000f;
		float height = 12000f;
		api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);
		
		float minX = -width/2;
		float minY = -height/2;
		
		// Add an asteroid field
		api.addAsteroidField(minX, minY + height / 2, 0, 8000f,
							 20f, 70f, 100);

		api.addPlanet(0, 0, 200f, "barren", 0f, true);
		
	}

}
