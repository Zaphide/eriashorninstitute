package data.missions.random_vs_esi;

import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import data.missions.BaseRandomEsiMissionDefinition;

public class MissionDefinition extends BaseRandomEsiMissionDefinition{

	public void defineMission(MissionDefinitionAPI api) {

		chooseFactions(null, "esi");
		super.defineMission(api);

	}

}
