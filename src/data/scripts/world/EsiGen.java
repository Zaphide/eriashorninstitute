package data.scripts.world;

import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import data.scripts.world.systems.Esira;

public class EsiGen {

    public void generate(SectorAPI sector) {
        new Esira().generate(sector);

        initFactionRelationships(sector);
    }

    public static void initFactionRelationships(SectorAPI sector) {
        FactionAPI player = sector.getFaction(Factions.PLAYER);

        FactionAPI esi = sector.getFaction("esi");

        // Vanilla Factions
        FactionAPI hegemony = sector.getFaction(Factions.HEGEMONY);
        FactionAPI tritachyon = sector.getFaction(Factions.TRITACHYON);
        FactionAPI pirates = sector.getFaction(Factions.PIRATES);
        FactionAPI independent = sector.getFaction(Factions.INDEPENDENT);
        FactionAPI kol = sector.getFaction(Factions.KOL);
        FactionAPI church = sector.getFaction(Factions.LUDDIC_CHURCH);
        FactionAPI path = sector.getFaction(Factions.LUDDIC_PATH);
        FactionAPI diktat = sector.getFaction(Factions.DIKTAT);
        FactionAPI league = sector.getFaction(Factions.PERSEAN);
        FactionAPI remnants = sector.getFaction(Factions.REMNANTS);

        // SET RELATIONSHIPS
        player.setRelationship(esi.getId(), 0);

        // ESI -> Vanilla
        esi.setRelationship(hegemony.getId(), RepLevel.SUSPICIOUS);
        esi.setRelationship(tritachyon.getId(), RepLevel.FRIENDLY);
        esi.setRelationship(pirates.getId(), RepLevel.HOSTILE);
        esi.setRelationship(independent.getId(), RepLevel.NEUTRAL);
        esi.setRelationship(church.getId(), RepLevel.HOSTILE);
        esi.setRelationship(path.getId(), RepLevel.HOSTILE);
        esi.setRelationship(diktat.getId(), RepLevel.FRIENDLY);
        esi.setRelationship(league.getId(), RepLevel.NEUTRAL);

        // Vanilla - > ESI
        hegemony.setRelationship(esi.getId(), RepLevel.SUSPICIOUS);
        tritachyon.setRelationship(esi.getId(), RepLevel.FRIENDLY);
        pirates.setRelationship(esi.getId(), RepLevel.HOSTILE);
        independent.setRelationship(esi.getId(), RepLevel.NEUTRAL);
        church.setRelationship(esi.getId(), RepLevel.HOSTILE);
        path.setRelationship(esi.getId(), RepLevel.HOSTILE);
        diktat.setRelationship(esi.getId(), RepLevel.FRIENDLY);
        league.setRelationship(esi.getId(), RepLevel.NEUTRAL);
    }
}