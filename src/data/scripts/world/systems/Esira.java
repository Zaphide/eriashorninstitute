package data.scripts.world.systems;

import java.awt.Color;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.JumpPointAPI;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.OrbitAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.StarTypes;
import com.fs.starfarer.api.impl.campaign.ids.Terrain;
import com.fs.starfarer.api.impl.campaign.procgen.StarAge;
import com.fs.starfarer.api.impl.campaign.terrain.BaseTiledTerrain;
import com.fs.starfarer.api.impl.campaign.terrain.MagneticFieldTerrainPlugin.MagneticFieldParams;
import com.fs.starfarer.api.util.Misc;

public class Esira {

	public void generate(SectorAPI sector) {
		
		StarSystemAPI system = sector.createStarSystem("Esira");
		LocationAPI hyper = Global.getSector().getHyperspace();
		
		system.setBackgroundTextureFilename("graphics/backgrounds/background5.jpg");
		
		SectorEntityToken esira_nebula = Misc.addNebulaFromPNG("data/campaign/terrain/hybrasil_nebula.png",
				  0, 0, // center of nebula
				  system, // location to add to
				  "terrain", "nebula_blue", // "nebula_blue", // texture to use, uses xxx_map for map
				  4, 4, StarAge.YOUNG); // number of cells in texture
		
		// create the star and generate the hyperspace anchor for this system
		PlanetAPI esira_Star = system.initStar("esira", // unique id for this star
											StarTypes.BLUE_GIANT,  // id in planets.json
										    800f, 		  // radius (in pixels at default zoom)
										    900); // corona radius, from star edge

		system.setLightColor(new Color(225, 245, 255)); // light color in entire system, affects all entities
		
//		SectorEntityToken culannStation = system.addCustomEntity("culann_starforge", "Culann Starforge", "station_side07", "tritachyon");
//		culannStation.setCircularOrbitPointingDown(system.getEntityById("culann"), 0, 250, 30);
//		culannStation.setInteractionImage("illustrations", "orbital");
//		culannStation.setCustomDescriptionId("station_culann");

		// Esira System Relay
		SectorEntityToken relay = system.addCustomEntity("esira_relay", // unique id
				"Esira Relay", // name - if null, defaultName from custom_entities.json will be used
				"comm_relay", // type of object, defined in custom_entities.json
				"esi"); // faction

		relay.setCircularOrbitPointingDown( system.getEntityById("esira"), 320, 2400, 400);

		SectorEntityToken esiraLP = system.addCustomEntity(null,"Esira Isolated Listening Station", "sensor_array_makeshift", "esi");
		esiraLP.setCircularOrbitPointingDown( esira_Star, 120, 9500, 820);

		// An asteroid field
		system.addRingBand(esira_Star, "misc", "rings_asteroids0", 256f, 0, Color.blue, 256f, 4300, 220f, null, null);
		system.addRingBand(esira_Star, "misc", "rings_asteroids0", 256f, 1, Color.blue, 256f, 4400, 226f, null, null);
		system.addAsteroidBelt(esira_Star, 60, 4350, 170, 200, 250, Terrain.ASTEROID_BELT, "Esira Reef");
		system.addRingBand(esira_Star, "misc", "rings_ice0", 256f, 0, Color.white, 256f, 4500, 200f);
		system.addRingBand(esira_Star, "misc", "rings_ice0", 256f, 2, Color.white, 256f, 4600, 240f);
		system.addRingBand(esira_Star, "misc", "rings_ice0", 256f, 1, Color.white, 256f, 4700, 260f);
		system.addRingBand(esira_Star, "misc", "rings_ice0", 256f, 2, Color.white, 256f, 4800, 280f);

		// Esira I system
		PlanetAPI casey = system.addPlanet("esi_casey", esira_Star, "Casey", "barren", 10, 110, 2800, 125);
		casey.getSpec().setTexture(Global.getSettings().getSpriteName("planets", "castiron"));
		casey.getSpec().setPlanetColor(new Color(220,235,245,255));
		casey.setCustomDescriptionId("esi_planet_casey");
		casey.setInteractionImage("illustrations", "mine");
		casey.applySpecChanges();

		// Esira II System
		PlanetAPI esira2 = system.addPlanet("esira2", esira_Star, "Esira II", "ice_giant", 20, 400, 7300, 500);
		esira2.getSpec().setPlanetColor(new Color(255,245,215,255));
		esira2.getSpec().setAtmosphereColor(new Color(220,250,255,150));
		esira2.getSpec().setCloudColor(new Color(220,250,255,200));
		esira2.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "aurorae"));
		esira2.getSpec().setGlowColor(new Color(0,220,245,64));
		esira2.getSpec().setIconColor(new Color(250,225,205,255));
		esira2.applySpecChanges();

		SectorEntityToken esira2_nebula = system.addTerrain(Terrain.NEBULA, new BaseTiledTerrain.TileParams(
				"          " +
						" x   xxxx " +
						"   xxx    " +
						"  xx  xx  " +
						"  xxxxx   " +
						"  xxxxx x " +
						"   xxxx   " +
						"x  xxxxx  " +
						"  xxxxxxx " +
						"    xxx   ",
				10, 10, // size of the nebula grid, should match above string
				"terrain", "nebula_blue", 4, 4, null));
		esira2_nebula.setCircularOrbit(esira_Star, 20, 7300, 500);

			// & the moons of Esira II
			system.addRingBand(esira2, "misc", "rings_dust0", 256f, 3, Color.white, 256f, 875, 33f, Terrain.RING, null);
			system.addRingBand(esira2, "misc", "rings_dust0", 256f, 4, Color.white, 256f, 1050, 33f, Terrain.RING, null);

			SectorEntityToken esira2_field = system.addTerrain(Terrain.MAGNETIC_FIELD,
					new MagneticFieldParams(esira2.getRadius() + 150f, // terrain effect band width
							(esira2.getRadius() + 150f) / 2f, // terrain effect middle radius
							esira2, // entity that it's around
							esira2.getRadius() + 50f, // visual band start
							esira2.getRadius() + 50f + 300f, // visual band end
							new Color(50, 20, 100, 50), // base color
							0.5f, // probability to spawn aurora sequence, checked once/day when no aurora in progress
							new Color(90, 180, 40),
							new Color(130, 145, 90),
							new Color(165, 110, 145),
							new Color(95, 55, 160),
							new Color(45, 0, 130),
							new Color(20, 0, 130),
							new Color(10, 0, 150)));

			esira2_field.setCircularOrbit(esira2, 0, 0, 100);

			PlanetAPI davis = system.addPlanet("esi_davis", esira2, "Davis", "tundra", 30, 140, 1550, 40);
			davis.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "volturn"));
			davis.getSpec().setGlowColor(new Color(255,255,255,255));
			davis.getSpec().setUseReverseLightForGlow(true);
			davis.applySpecChanges();
			davis.setCustomDescriptionId("esi_planet_davis");
			davis.setInteractionImage("illustrations", "urban03");
			
		// Eochu Bres mirror system
//		SectorEntityToken eochu_bres_mirror1 = system.addCustomEntity("eochu_bres_mirror1", "Eochu Bres Stellar Mirror", "stellar_mirror", "tritachyon");
//		eochu_bres_mirror1.setCircularOrbitPointingDown(system.getEntityById("eochu_bres"), 0, 220, 40);
//		eochu_bres_mirror1.setCustomDescriptionId("stellar_mirror");
//
//		SectorEntityToken eochu_bres_mirror2 = system.addCustomEntity("eochu_bres_mirror2", "Eochu Bres Stellar Mirror", "stellar_mirror", "tritachyon");
//		eochu_bres_mirror2.setCircularOrbitPointingDown(system.getEntityById("eochu_bres"), 120, 220, 40);
//		eochu_bres_mirror2.setCustomDescriptionId("stellar_mirror");
//
//		SectorEntityToken eochu_bres_mirror3 = system.addCustomEntity("eochu_bres_mirror3", "Eochu Bres Stellar Mirror", "stellar_mirror", "tritachyon");
//		eochu_bres_mirror3.setCircularOrbitPointingDown(system.getEntityById("eochu_bres"), 240, 220, 40);
//		eochu_bres_mirror3.setCustomDescriptionId("stellar_mirror");
				
//		PlanetAPI hybrasil2b = system.addPlanet("ogma", hybrasil2, "Ogma", "rocky_metallic", 40, 100, 2050, 56);
		//hybrasil2b.setCustomDescriptionId("planet_ogma");

		//SectorEntityToken hybrasil_station = system.addCustomEntity("hybrasil_station", "Hybrasil Astropolis", "station_side03", "tritachyon");
		//hybrasil_station.setCircularOrbitPointingDown(system.getEntityById("ogma"), 90, 220, 25);
		//hybrasil_station.setCustomDescriptionId("station_ogma");
		//hybrasil_station.setInteractionImage("illustrations", "orbital");
		
		
		// Esira III
		PlanetAPI mawson = system.addPlanet("esi_mawson", esira_Star, "Mawson", "rocky_ice", 235, 180, 12000, 340);
		mawson.setInteractionImage("illustrations", "cargo_loading");
//		esira3.getSpec().setTexture(Global.getSettings().getSpriteName("hab_glows", "volturn"));
//		esira3.getSpec().setPlanetColor(new Color(185,240,255,255));
//		esira3.applySpecChanges();
		mawson.setCustomDescriptionId("esi_planet_mawson");
		//		hybrasil3.setInteractionImage("illustrations", "mine");
		
//		Misc.initConditionMarket(hybrasil3);
//		hybrasil3.getMarket().addCondition(Conditions.DECIVILIZED);
//		hybrasil3.getMarket().addCondition(Conditions.RUINS_SCATTERED);
//		hybrasil3.getMarket().getFirstCondition(Conditions.RUINS_SCATTERED).setSurveyed(true);
//		hybrasil3.getMarket().addCondition(Conditions.ORE_MODERATE);
//		hybrasil3.getMarket().addCondition(Conditions.RARE_ORE_MODERATE);
//		hybrasil3.getMarket().addCondition(Conditions.HOT);
//		hybrasil3.getMarket().addCondition(Conditions.THIN_ATMOSPHERE);
		
		
//		SectorEntityToken crom_cruach_loc = system.addCustomEntity(null,null, "sensor_array_makeshift",Factions.TRITACHYON);
//		crom_cruach_loc.setCircularOrbitPointingDown( esi_star, 180-60, 7300, 340);
//
//		// jump point Crom Leim!
//		JumpPointAPI jumpPoint = Global.getFactory().createJumpPoint("hybrasil_inner_jump", "Hybrasil Inner System Jump-point");
//		OrbitAPI orbit = Global.getFactory().createCircularOrbit(hybrasil3, 0, 1500, 65);
//		jumpPoint.setOrbit(orbit);
//		jumpPoint.setRelatedPlanet(hybrasil3);
//		jumpPoint.setStandardWormholeToHyperspaceVisual();
//		system.addEntity(jumpPoint);
//
		// Esira IV System
		PlanetAPI esira4 = system.addPlanet("esira4", esira_Star, "Esira IV", "ice_giant", 60, 340, 16000, 800);
		esira4.getSpec().setPlanetColor(new Color(255,245,215,255));
		esira4.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "aurorae"));
		esira4.getSpec().setGlowColor(new Color(0,255,205,64));
		esira4.getSpec().setUseReverseLightForGlow(true);
		esira4.applySpecChanges();
		//esira4.setCustomDescriptionId("planet_balar");

		SectorEntityToken esira4_nebula = system.addTerrain(Terrain.NEBULA, new BaseTiledTerrain.TileParams(
				"          " +
						" x   xxxx " +
						"   xxx    " +
						"  xx  xx  " +
						"  xxxxx   " +
						"  xxxxx x " +
						"   xxxx   " +
						"x  xxxxx  " +
						"  xxxxxxx " +
						"    xxx   ",
				10, 10, // size of the nebula grid, should match above string
				"terrain", "nebula_blue", 4, 4, null));
		esira4_nebula.setCircularOrbit(esira_Star, 60, 16000, 800);
		
			system.addRingBand(esira4, "misc", "rings_ice0", 256f, 2, Color.white, 256f, 650, 21f, Terrain.RING, null);
		
			SectorEntityToken esira4_field = system.addTerrain(Terrain.MAGNETIC_FIELD,
			new MagneticFieldParams(esira4.getRadius() + 150f, // terrain effect band width
					(esira4.getRadius() + 150f) / 2f, // terrain effect middle radius
					esira4, // entity that it's around
					esira4.getRadius() + 50f, // visual band start
					esira4.getRadius() + 50f + 200f, // visual band end
					new Color(50, 20, 100, 50), // base color
					0.5f, // probability to spawn aurora sequence, checked once/day when no aurora in progress
					new Color(90, 180, 40),
					new Color(130, 145, 90),
					new Color(165, 110, 145),
					new Color(95, 55, 160),
					new Color(45, 0, 130),
					new Color(20, 0, 130),
					new Color(10, 0, 150)));

			esira4_field.setCircularOrbit(esira4, 0, 0, 100);
		
			// the moons of Esira IV
			PlanetAPI esira4a = system.addPlanet("esira4a", esira4, "Esira IV A", "rocky_ice", 0, 70, 900, 15);
			//esira4a.setCustomDescriptionId("planet_ena");

			PlanetAPI jinnah = system.addPlanet("esi_jinnah", esira4, "Jinnah", "cryovolcanic", 70, 120, 1300, 30);
			jinnah.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "banded"));
			jinnah.getSpec().setGlowColor(new Color(0,255,205,64));
			jinnah.getSpec().setUseReverseLightForGlow(true);
			jinnah.applySpecChanges();
			jinnah.setInteractionImage("illustrations", "pirate_station");
			jinnah.setCustomDescriptionId("esi_planet_jinnah");

			PlanetAPI esira4c = system.addPlanet("esira4c", esira4, "Esira IV C", "frozen", 140, 80, 1580, 60);
			esira4c.getSpec().setTexture(Global.getSettings().getSpriteName("planets", "frozen00"));
			esira4c.applySpecChanges();

			PlanetAPI esira4d = system.addPlanet("esira4d", esira4, "Esira IV D", "barren-bombarded", 210, 50, 1800, 90);
			esira4d.getSpec().setTexture(Global.getSettings().getSpriteName("planets", "barren02"));
			esira4d.applySpecChanges();

		PlanetAPI esira5 = system.addPlanet("esira5", esira_Star, "Esira V", "barren", 180, 140, 16000, 800);
		//esira5.setCustomDescriptionId("planet_donn");
		//esira5.setInteractionImage("illustrations", "pirate_station");
		
		// generates hyperspace destinations for in-system jump points
		system.autogenerateHyperspaceJumpPoints(true, true);
	}
}
