package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import data.scripts.world.EsiGen;
import exerelin.campaign.SectorManager;

public class EsiModPlugin extends BaseModPlugin {

    @Override
    public void onNewGame() {
        boolean isNexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
        if (isNexerelin && !SectorManager.getCorvusMode())
        {
            return;
        }
        this.initEsi();
    }

    private void initEsi() {
        new EsiGen().generate(Global.getSector());
    }
}