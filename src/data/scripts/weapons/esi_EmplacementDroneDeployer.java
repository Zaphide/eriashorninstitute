package data.scripts.weapons;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import org.lazywizard.lazylib.combat.CombatUtils;

public class esi_EmplacementDroneDeployer implements EveryFrameWeaponEffectPlugin
{
    private static final String PROJECTILE_SPEC = "esi_emplacement_drone_capsule";  // Projectile to transform
    private static final float FLIGHT_TIME = 5f;                                    // Flight time of projectile before transforming, should match weapon spec
    private static final String WING_TO_SPAWN = "esi_addendum_wing";             // Ship variant to spawn
    private static final float STARTING_CR = 0.6f;                                  // CR of ship on spawn
    private static final String SHIP_NAME = "Bolter Gun Wing";                    // Name of spawned ship

    private int lastWeaponAmmo = 0;
    private float lastElapsed = 0f;
    private boolean inProgress = false;

    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon)
    {
        if (engine.isPaused()) {
            return; // Don't do anything
        }

        int weapon_ammo = weapon.getAmmo();

        if (weapon_ammo < lastWeaponAmmo) {
            // Weapon has fired, so treat as in progress
            inProgress = true;
            lastWeaponAmmo = weapon_ammo;
        }
        else if (!inProgress && weapon_ammo > lastWeaponAmmo)
        {
            // Weapon has reloaded, so reset weapon and timer
            lastElapsed = 0;
            lastWeaponAmmo = weapon_ammo;
        }

        if (inProgress)
        {
            for (DamagingProjectileAPI proj : engine.getProjectiles())
            {
                if(proj == null || proj.getProjectileSpecId() == null || !proj.getProjectileSpecId().equalsIgnoreCase(PROJECTILE_SPEC))
                    continue; // Skip projectiles we don't care about

                if(proj == null || !proj.getWeapon().equals(weapon))
                    continue; // Skip projectiles that weren't fired by this weapon

                // Found capsule projectile fired from this weapon, so slow down
                float elapsed = proj.getElapsed();
                float timeLeftToStop = (elapsed - lastElapsed) / (FLIGHT_TIME / 2);
                float xVelocity = proj.getVelocity().getX();
                float yVelocity = proj.getVelocity().getY();
                float xVelIncrement = xVelocity * timeLeftToStop;
                float yVelIncrement = yVelocity * timeLeftToStop;
                float xChange = xVelocity - xVelIncrement;
                float yChange = yVelocity - yVelIncrement;

                // TODO curve missiles in towards ship again to 'level out'

                //Global.getLogger(this.getClass()).info("Elapsed: " + elapsed + ", Vel_x: " + xVelocity + ", xVelIncrement: " + xVelIncrement + ", xChange: " + xChange);
                //Global.getLogger(this.getClass()).info("Elapsed: " + elapsed + ", Vel_y: " + yVelocity + ", yVelIncrement: " + yVelIncrement + ", yChange: " + yChange);
                proj.getVelocity().set(xChange, yChange);

                if(elapsed >= FLIGHT_TIME)
                {
                    // Flight has ended so transform projectile into drone wing
                    engine.getFleetManager(weapon.getShip().getOwner()).setSuppressDeploymentMessages(true);
                    ShipAPI deployedDroneWing = engine.getFleetManager(weapon.getShip().getOwner()).spawnShipOrWing(WING_TO_SPAWN, proj.getLocation(), weapon.getShip().getFacing());
                    //ShipAPI deployedDroneWing = CombatUtils.spawnShipOrWingDirectly(WING_TO_SPAWN, FleetMemberType.FIGHTER_WING, FleetSide.PLAYER, STARTING_CR, proj.getLocation(), weapon.getShip().getFacing())
                    engine.getFleetManager(weapon.getShip().getOwner()).setSuppressDeploymentMessages(false);

                    deployedDroneWing.setCurrentCR(STARTING_CR);
                    deployedDroneWing.setCRAtDeployment(STARTING_CR);
                    deployedDroneWing.getShipAI().forceCircumstanceEvaluation();

                    if(weapon.getShip().getShipTarget() != null)
                        deployedDroneWing.setShipTarget(weapon.getShip().getShipTarget());

                    FleetMemberAPI fleetMemberAPI = CombatUtils.getFleetMember(deployedDroneWing);
                    if(fleetMemberAPI != null)
                        fleetMemberAPI.setShipName(SHIP_NAME);

                    if(weapon.getShip().getShipTarget() != null)
                        deployedDroneWing.setShipTarget(weapon.getShip().getShipTarget());

                    engine.removeEntity(proj);
                    inProgress = false;
                    break;
                }

                lastElapsed = elapsed;
            }
        }
    }
}
