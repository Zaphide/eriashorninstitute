package data.scripts.weapons;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import org.lazywizard.lazylib.combat.CombatUtils;

public class esi_EmplacementDroneSmallDeployer implements EveryFrameWeaponEffectPlugin
{
    private static final String PROJECTILE_SPEC = "esi_emplacement_drone_capsule_small";
    private static final float FLIGHT_TIME = 5f;
    private static final String WING_TO_SPAWN = "esi_addendum_wing";
    private static final float STARTING_CR = 0.6f;                                  // CR of ship on spawn
    private static final String SHIP_NAME = "Bolter Gun Wing";                      // Name of spawned ship

    private int lastWeaponAmmo = 0;
    private float lastElapsed = 0f;
    private boolean inProgress = false;
    
    public boolean overrideSpawn = false; // Used to cancel this weapon (as spawning a two member wing only once from two weapons fired)

    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        // Don't bother with any checks if the game is paused
        if (engine.isPaused()) {
            return;
        }

        int weapon_ammo = weapon.getAmmo();

        if (weapon_ammo < lastWeaponAmmo)
        {
            // Weapon has fired, so treat as in progress
            inProgress = true;
            lastWeaponAmmo = weapon_ammo;
        }
        else if (!inProgress && weapon_ammo > lastWeaponAmmo)
        {
            // Weapon has reloaded, so reset weapon
            lastElapsed = 0;
            lastWeaponAmmo = weapon_ammo;
        }

        if (inProgress)
        {
            boolean projFound = false;
            for (DamagingProjectileAPI proj : engine.getProjectiles())
            {
                if(proj == null || proj.getProjectileSpecId() == null || !proj.getProjectileSpecId().equalsIgnoreCase(PROJECTILE_SPEC))
                    continue; // Skip projectiles we don't care about

                if(!proj.getWeapon().equals(weapon))
                    continue; // Skip projectiles that weren't fired by this weapon

                projFound = true;

                // Found capsule projectile fired from this weapon, so slow down
                float elapsed = proj.getElapsed();
                float timeLeftToStop = (elapsed - lastElapsed) / (FLIGHT_TIME / 2);
                float xVelocity = proj.getVelocity().getX();
                float yVelocity = proj.getVelocity().getY();
                float xVelIncrement = xVelocity * timeLeftToStop;
                float yVelIncrement = yVelocity * timeLeftToStop;
                float xChange = xVelocity - xVelIncrement;
                float yChange = yVelocity - yVelIncrement;

                // TODO curve missiles in towards ship again to 'level out'

                //Global.getLogger(this.getClass()).info("Elapsed: " + elapsed + ", Vel_x: " + xVelocity + ", xVelIncrement: " + xVelIncrement + ", xChange: " + xChange);
                //Global.getLogger(this.getClass()).info("Elapsed: " + elapsed + ", Vel_y: " + yVelocity + ", yVelIncrement: " + yVelIncrement + ", yChange: " + yChange);
                proj.getVelocity().set(xChange, yChange);

                if(elapsed >= FLIGHT_TIME && !overrideSpawn)
                {
                    // Flight has ended so transform projectile into drone wing
                    engine.getFleetManager(weapon.getShip().getOwner()).setSuppressDeploymentMessages(true);
                    ShipAPI deployedDroneWing = engine.getFleetManager(weapon.getShip().getOwner()).spawnShipOrWing(WING_TO_SPAWN, proj.getLocation(), weapon.getShip().getFacing());
                    engine.getFleetManager(weapon.getShip().getOwner()).setSuppressDeploymentMessages(false);

                    deployedDroneWing.setCurrentCR(STARTING_CR);
                    deployedDroneWing.setCRAtDeployment(STARTING_CR);
                    deployedDroneWing.getShipAI().forceCircumstanceEvaluation();

                    FleetMemberAPI fleetMemberAPI = CombatUtils.getFleetMember(deployedDroneWing);
                    if(fleetMemberAPI != null)
                        fleetMemberAPI.setShipName(SHIP_NAME);

                    if(weapon.getShip().getShipTarget() != null)
                        deployedDroneWing.setShipTarget(weapon.getShip().getShipTarget());

                    // Weapon is in pair, so find other projectile from other weapon on same ship and if exists, remove
                    int projCount = 0;
                    for (DamagingProjectileAPI proj2 : engine.getProjectiles())
                    {
                        if(proj2 == null || proj2.getProjectileSpecId() == null || !proj2.getProjectileSpecId().equalsIgnoreCase(PROJECTILE_SPEC))
                            continue; // Skip projectiles we don't care about

                        if(proj2.getWeapon().equals(weapon) || !proj.getWeapon().getShip().equals(proj2.getWeapon().getShip()))
                            continue; // Skip projectiles that were fired by this weapon, or by a different ship

                        // Found projectile, so move next wing member to location and remove projectile. Set override on launching weapon.
                        projCount++;
                        deployedDroneWing.getWing().getWingMembers().get(projCount).getLocation().set(proj2.getLocation().getX(), proj2.getLocation().getY());
                        engine.removeEntity(proj2);
                        ((esi_EmplacementDroneSmallDeployer)proj2.getWeapon().getEffectPlugin()).overrideSpawn = true;
                    }

                    // If 2nd projectile destroyed, remove one wing member
                    if(projCount == 0)
                        engine.removeEntity(deployedDroneWing.getWing().getWingMembers().get(1));

                    engine.removeEntity(proj);
                    inProgress = false;
                    break;
                }

                lastElapsed = elapsed;
            }

            if(!projFound && (lastElapsed >= FLIGHT_TIME + 1 || overrideSpawn))
            {
                // No projectiles found so assume proj destroyed / converted to wing
                overrideSpawn = false;
                inProgress = false;
            }
        }
    }
}
