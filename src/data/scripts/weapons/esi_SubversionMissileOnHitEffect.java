package data.scripts.weapons;

import com.fs.starfarer.api.combat.*;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public class esi_SubversionMissileOnHitEffect implements OnHitEffectPlugin {

    private static final float SUBVERSION_MISSILE_MINIMUM_CONVERSION_CHANCE = 0.25f;

    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target,
                      Vector2f point, boolean shieldHit, CombatEngineAPI engine) {

        if (target instanceof MissileAPI) {
            float subversionChance = SUBVERSION_MISSILE_MINIMUM_CONVERSION_CHANCE;

            if(projectile.getHitpoints() >= target.getHitpoints())
                subversionChance = 1.0f;
            else {
                subversionChance = Math.max(subversionChance, projectile.getHitpoints() / target.getHitpoints());
            }

            if(Math.random() < subversionChance) {
                target.setOwner(projectile.getOwner());

                ((MissileAPI) target).setSource(projectile.getSource()); // Need to do this to re-set collision stuff
                //target.setCollisionClass(CollisionClass.MISSILE_NO_FF);

                engine.spawnExplosion(point, target.getVelocity(), Color.YELLOW, 25, 0.4f);
                engine.removeEntity(projectile);
            }
        }
        else if(target instanceof ShipAPI && !shieldHit) {
            engine.spawnEmpArc( projectile.getSource(),
                                point,
                                target,
                                target,
                                DamageType.ENERGY,
                                1,
                                1,
                                0.5f,
                                "system_emp_emitter_impact",
                                1,
                                Color.YELLOW,
                                Color.WHITE);
        }
    }
}
