package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.util.IntervalUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class esi_PdDroneBayHullmodPlugin implements EveryFrameCombatPlugin {

    // See esi_PdDroneBay for hullmod stats

    private static final String HULLMOD_ID = "esi_pd_drone_bay";

    private static final float REGEN_INTERVAL = 60f;
    private static final String WING_TO_SPAWN = "esi_pd_drone_wing";

    // Number of drones per hull size
    private static Map dronesHullSize = new HashMap();
    static {
        dronesHullSize.put(ShipAPI.HullSize.FRIGATE, 1);
        dronesHullSize.put(ShipAPI.HullSize.DESTROYER, 2);
        dronesHullSize.put(ShipAPI.HullSize.CRUISER, 3);
        dronesHullSize.put(ShipAPI.HullSize.CAPITAL_SHIP, 4);
    }

    // Store combat instance related data
    Map<ShipAPI, List<ShipAPI>> dronesForShips = new HashMap();
    Map<ShipAPI, Integer> dronesInStorageForShips = new HashMap();
    Map<ShipAPI, CombatFleetManagerAPI.AssignmentInfo> assignmentsForShips = new HashMap();
    Map<ShipAPI, Float> lastRegenForShips = new HashMap();

    private final IntervalUtil tracker = new IntervalUtil(0.5f, 1f);
    private float totalAmount;

    public void advance(float amount, List events) {

//        if(Global.getCombatEngine().isPaused())
//            return;
//
//        this.tracker.advance(amount);
//        this.totalAmount += amount;
//
//        if (!tracker.intervalElapsed()) {
//            return;
//        }
//
//        this.checkShips();
//        this.removeDrones();
//        this.regenDrones();
//        this.deployDrones();
    }

    private void checkShips() {

        for(ShipAPI shipAPI : Global.getCombatEngine().getShips())
        {
            if(shipAPI.isFighter())
                continue; // Skip fighters

            if(shipAPI.getVariant().getHullMods().contains(HULLMOD_ID))
            {
                // Check if ship is already being tracked
                if(!this.dronesForShips.containsKey(shipAPI)) {
                    Global.getLogger(this.getClass()).info("adding ship");
                    this.dronesForShips.put(shipAPI, new ArrayList());
                    this.dronesInStorageForShips.put(shipAPI, (int)dronesHullSize.get(shipAPI.getHullSize()));
                    this.assignmentsForShips.put(shipAPI, this.createEscortAssignmentForShip(shipAPI));
                    this.lastRegenForShips.put(shipAPI, this.totalAmount);
                }

                // Store the escort assignment
                if(this.assignmentsForShips.get(shipAPI) == null)
                {
                    this.assignmentsForShips.remove(shipAPI);
                    this.assignmentsForShips.put(shipAPI, this.createEscortAssignmentForShip(shipAPI));
                }
            }
        }
    }

    private void removeDrones() {

        for(ShipAPI shipAPI : this.dronesForShips.keySet()){

            List<ShipAPI> drones = this.dronesForShips.get(shipAPI);
            List<ShipAPI> dronesToRemove = new ArrayList<>();

            for(ShipAPI drone : drones)
            {
                if(!drone.isAlive() || drone.isHulk()) {
                    Global.getLogger(this.getClass()).info("removing drone");
                    dronesToRemove.add(drone);
                }
            }

            for(ShipAPI drone : dronesToRemove)
                drones.remove(drone);
        }
    }

    private void regenDrones() {

        for(ShipAPI shipAPI : this.dronesForShips.keySet()) {
            int dronesInStorage = this.dronesInStorageForShips.get(shipAPI);
            float lastRegen = this.lastRegenForShips.get(shipAPI);

            if(this.totalAmount > lastRegen + REGEN_INTERVAL) {
                if (dronesInStorage < (int) dronesHullSize.get(shipAPI.getHullSize())) {
                    Global.getLogger(this.getClass()).info("regenning drone");
                    dronesInStorage++;
                    this.dronesInStorageForShips.remove(shipAPI);
                    this.dronesInStorageForShips.put(shipAPI, dronesInStorage);
                }

                lastRegen = this.totalAmount; // ncrement this so that drones are not re-genned immediately after a deployment
                this.lastRegenForShips.remove(shipAPI);
                this.lastRegenForShips.put(shipAPI, lastRegen);
            }
        }
    }

    private void deployDrones(){

        for(ShipAPI shipAPI : this.dronesForShips.keySet()){

            // Get data for ship with drone bay hullmod
            List<ShipAPI> drones = this.dronesForShips.get(shipAPI);
            int dronesInStorage = this.dronesInStorageForShips.get(shipAPI);
            CombatFleetManagerAPI.AssignmentInfo assignmentInfo = this.assignmentsForShips.get(shipAPI);

            // Create drone if required
            if(drones.size() < (int)dronesHullSize.get(shipAPI.getHullSize()) && dronesInStorage > 0){
                Global.getLogger(this.getClass()).info("deploying drone");
                ShipAPI deployedDrone = Global.getCombatEngine().getFleetManager(shipAPI.getOwner()).spawnShipOrWing(WING_TO_SPAWN, shipAPI.getLocation(), shipAPI.getFacing());
                drones.add(deployedDrone);

                // TODO replace with escort drone AI
                //deployedDrone.setShipAI(new esi_EscortDroneAI(deployedDrone, shipAPI));
                //deployedDrone.getShipAI().forceCircumstanceEvaluation();

                // Assign to escort order
                CombatFleetManagerAPI combatFleetManagerAPI = Global.getCombatEngine().getFleetManager(shipAPI.getOwner());
                combatFleetManagerAPI.getTaskManager(false).giveAssignment(this.getDeployedFleetMemberAPI(deployedDrone), assignmentInfo, false);

                // Remove drone from storage
                dronesInStorage--;
                this.dronesInStorageForShips.remove(shipAPI);
                this.dronesInStorageForShips.put(shipAPI, dronesInStorage);
            }
        }
    }

    private CombatFleetManagerAPI.AssignmentInfo createEscortAssignmentForShip(ShipAPI shipAPI) {
        CombatFleetManagerAPI combatFleetManagerAPI = Global.getCombatEngine().getFleetManager(shipAPI.getOwner());
        CombatFleetManagerAPI.AssignmentInfo assignmentInfo = combatFleetManagerAPI.getTaskManager(false).createAssignment(CombatAssignmentType.LIGHT_ESCORT, (AssignmentTargetAPI)this.getDeployedFleetMemberAPI(shipAPI), false);

        return assignmentInfo;
    }

    private DeployedFleetMemberAPI getDeployedFleetMemberAPI(ShipAPI shipAPI) {
        CombatFleetManagerAPI fm = Global.getCombatEngine().getFleetManager(shipAPI.getOriginalOwner());
        return fm.getDeployedFleetMember(shipAPI);
    }

    public void renderInUICoords(ViewportAPI viewport) {

    }

    public void init(CombatEngineAPI engine) {

    }

    public void renderInWorldCoords(ViewportAPI viewport) {

    }

    public void processInputPreCoreControls(float amount, List<InputEventAPI> events) {

    }

}
