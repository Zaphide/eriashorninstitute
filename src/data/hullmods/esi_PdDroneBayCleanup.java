package data.hullmods;

public class esi_PdDroneBayCleanup extends esi_BaseDroneBayCleanup {

    protected String getHullModId() { return "esi_pd_drone_bay"; }
    protected String getWingId() { return "esi_pd_drone_wing"; };
    protected String getCleanUpHullModId() { return "esi_pd_drone_bay_cleanup"; };

}

