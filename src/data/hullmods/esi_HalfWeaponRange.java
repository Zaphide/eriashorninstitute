package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

public class esi_HalfWeaponRange extends BaseHullMod {

	public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
		stats.getEnergyWeaponRangeBonus().modifyMult(id, 0.5f);
		stats.getBallisticWeaponRangeBonus().modifyMult(id, 0.5f);
		stats.getMissileWeaponRangeBonus().modifyMult(id, 0.5f);
	}
	
	public String getDescriptionParam(int index, HullSize hullSize) {
		return null;
	}


}
