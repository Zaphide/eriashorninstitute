package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

public class esi_EnergyRedirect extends BaseHullMod {

	public static final float RANGE_BONUS_PERCENT = 50f;
	public static final float ROF_REDUCTION_PERCENT = -35f;
	
	public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
		stats.getEnergyWeaponRangeBonus().modifyPercent(id, RANGE_BONUS_PERCENT);
		stats.getEnergyRoFMult().modifyPercent(id, ROF_REDUCTION_PERCENT);
	}
	
	public String getDescriptionParam(int index, HullSize hullSize) {
		if (index == 0) return "" + (int) RANGE_BONUS_PERCENT;
		if (index == 1) return "" + (int) ROF_REDUCTION_PERCENT;
		return null;
	}


}
