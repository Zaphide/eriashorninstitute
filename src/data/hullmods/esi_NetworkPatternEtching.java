package data.hullmods;


import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.util.IntervalUtil;
//import com.fs.starfarer.loading.ShipHullSpecLoader;
import org.lazywizard.lazylib.combat.AIUtils;

public class esi_NetworkPatternEtching extends BaseHullMod {

    public static final float BONUS_PERCENT = 8f; // Default bonus
    public static final float FLEET_BONUS_PERCENT = 2f; // Additional bonus per fleet member, applied either in combat OR on compaign map

    private static final String hullModId = "esi_network_pattern_etching";

    private final IntervalUtil tracker = new IntervalUtil(3f, 6f);

    //private int numHullModInFleet = 0;

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        // Base apply
        stats.getSensorStrength().modifyPercent(hullModId, (int)(BONUS_PERCENT));
        stats.getEnergyWeaponRangeBonus().modifyPercent(hullModId, BONUS_PERCENT);
        stats.getSightRadiusMod().modifyPercent(hullModId, BONUS_PERCENT);
    }

    @Override
    public void advanceInCampaign(FleetMemberAPI member, float amount)
    {
        tracker.advance(amount);

        if (!tracker.intervalElapsed()) {
            return;
        }

        int numHullModInFleet = 0;

        for(FleetMemberAPI fleetMemberAPI : member.getFleetData().getCombatReadyMembersListCopy())
        {
            if(fleetMemberAPI.equals(member) || fleetMemberAPI.isFighterWing())
                continue;

            if(fleetMemberAPI.getVariant().hasHullMod(hullModId))
                numHullModInFleet++;
        }

        MutableShipStatsAPI stats = member.getStats();
        stats.getSensorStrength().modifyPercent(hullModId, (int)(BONUS_PERCENT + (FLEET_BONUS_PERCENT*numHullModInFleet)));
    }

    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {

        tracker.advance(amount);

        if (!tracker.intervalElapsed()) {
            return;
        }

        int hullModInCombatCount = 0;

        for(ShipAPI shipAPI: AIUtils.getAlliesOnMap(ship))
        {
            if(ship == shipAPI || shipAPI.isFighter())
                continue;

            if(shipAPI.getVariant().hasHullMod(hullModId))
                hullModInCombatCount++;
        }

        MutableShipStatsAPI stats = ship.getMutableStats();
        stats.getEnergyWeaponRangeBonus().modifyPercent(hullModId, BONUS_PERCENT + (FLEET_BONUS_PERCENT*hullModInCombatCount));
        stats.getSightRadiusMod().modifyPercent(hullModId, BONUS_PERCENT + (FLEET_BONUS_PERCENT*hullModInCombatCount));
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) return "" + (int) BONUS_PERCENT;
        if (index == 1) return "" + (int) FLEET_BONUS_PERCENT;
        return null;
    }


}