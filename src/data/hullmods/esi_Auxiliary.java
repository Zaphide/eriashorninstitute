package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;

public class esi_Auxiliary extends BaseHullMod {

    private static final float BONUS_MAX_FLUX_PERCENT = 5;
    private static final float BONUS_FLUX_DISSIPATION_PERCENT = 10;

    private static final float REDUCE_TOP_SPEED_PERCENT = -5;
    private static final float REDUCE_ARMOUR_SPEED_PERCENT = -5;
    private static final float REDUCE_CREW = -5;

    public void applyEffectsBeforeShipCreation(ShipAPI.HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getFluxCapacity().modifyPercent(id, BONUS_MAX_FLUX_PERCENT);
        stats.getFluxDissipation().modifyPercent(id, BONUS_FLUX_DISSIPATION_PERCENT);

        stats.getMaxSpeed().modifyPercent(id, REDUCE_TOP_SPEED_PERCENT);
        stats.getArmorBonus().modifyPercent(id, REDUCE_ARMOUR_SPEED_PERCENT);
        stats.getMinCrewMod().modifyPercent(id, REDUCE_CREW);
    }

    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize) {
        if (index == 0) return "" + (int) BONUS_MAX_FLUX_PERCENT + "%";
        if (index == 1) return "" + (int) BONUS_FLUX_DISSIPATION_PERCENT + "%";

        if (index == 2) return "" + (int) REDUCE_TOP_SPEED_PERCENT + "%";
        if (index == 3) return "" + (int) REDUCE_ARMOUR_SPEED_PERCENT + "%";
        if (index == 4) return "" + (int) REDUCE_CREW + "%";
        return null;
    }


}