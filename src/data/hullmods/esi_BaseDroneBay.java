package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.MutableStat;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.campaign.ids.HullMods;
import com.fs.starfarer.api.impl.campaign.ids.Stats;

import java.util.ArrayList;
import java.util.List;

// Add a built in wing and retain existing wing composition

// NOTE: This was originally intended to modify built in wings directly but discussed with Alex and modifying
// the hull spec like that will affect all new hulls generated from the spec (tested this and confirmed in the
// pre-mission setup). So, that method looks like it's a no-go for now :(

public abstract class esi_BaseDroneBay extends BaseHullMod {

    protected abstract String getHullModId();
    //protected abstract String getWingId();
    //protected abstract String getCleanUpHullModId();

    protected abstract String getRequiredHullMod();
    protected abstract String getOppositeHullMod();
    protected abstract float getRequiredCargoSpace();
    protected abstract float getNumFighterBaysToAdd();

    //private boolean isSetInCombat = false;
    //private String hullModId;

    public static final int ALL_FIGHTER_COST_PERCENT = 3000;

    //private int wingIndex;

    @Override
    public void applyEffectsBeforeShipCreation(ShipAPI.HullSize hullSize, MutableShipStatsAPI stats, String id) {
        //this.hullModId = id;

        stats.getCargoMod().modifyFlat(id, (-1 * getRequiredCargoSpace()));
        stats.getNumFighterBays().modifyFlat(id, getNumFighterBaysToAdd());

        stats.getDynamic().getMod(Stats.ALL_FIGHTER_COST_MOD).modifyPercent(id, ALL_FIGHTER_COST_PERCENT);
        stats.getDynamic().getMod(Stats.BOMBER_COST_MOD).modifyPercent(id, ALL_FIGHTER_COST_PERCENT);
        stats.getDynamic().getMod(Stats.FIGHTER_COST_MOD).modifyPercent(id, ALL_FIGHTER_COST_PERCENT);
        stats.getDynamic().getMod(Stats.INTERCEPTOR_COST_MOD).modifyPercent(id, ALL_FIGHTER_COST_PERCENT);
        stats.getDynamic().getMod(Stats.SUPPORT_COST_MOD).modifyPercent(id, ALL_FIGHTER_COST_PERCENT);
//
        if (stats.getEntity() instanceof ShipAPI) {
//            ShipAPI ship = (ShipAPI) stats.getEntity();
//            this.wingIndex = stats.getNumFighterBays().getModifiedInt() - 1;

            //numExistingWings = ship.getVariant().getNonBuiltInWings().size();
//
//            List<String> existingBuiltInWings = new ArrayList<String>(ship.getVariant().getHullSpec().getBuiltInWings());
//            List<String> existingFittedWings = ship.getVariant().getNonBuiltInWings(); // Note: Only works in BeforeShipCreation
//
//            // Check we haven't already added the wing as this can be called many times
//            if(existingBuiltInWings.contains(getWingId())) {
//                return;
//            }
//
//            // Clear any existing fitted wings so we can add the new built-in prior (as otherwise it locks them all)
//            ship.getVariant().getFittedWings().clear();
//
//            int nextWingIndexForAdd = existingBuiltInWings.size() > 0
//                    ? existingBuiltInWings.size() - 1
//                    : 0;
//
//            // Add new built-in wing (needs to be after existing built-ins but before to-be-moved fitted wings
//            ship.getVariant().setWingId(nextWingIndexForAdd, getWingId());
//            ship.getVariant().getHullSpec().getBuiltInWings().add(getWingId()); // Make it built-in
//            nextWingIndexForAdd++;
//
//            // Then, re-add the fitted wings
//            for(String wId : existingFittedWings) {
//                ship.getVariant().setWingId(nextWingIndexForAdd, wId);
//                nextWingIndexForAdd++;
//            }
        }
    }

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        // Add cleanup hullmod to watch for when this hullmod is removed and cleanup fighter wings
//        if(!ship.getVariant().hasHullMod(getCleanUpHullModId())){
//            ship.getVariant().addMod(getCleanUpHullModId());
//        }

        // NOTE: This will ensure the wing is always what is intended but it's a bit shit in the UI...
//        if(ship.getVariant().getWing(this.wingIndex) == null) {
//            ship.getVariant().setWingId(this.wingIndex, getWingId());
//        } else if (!ship.getVariant().getWing(this.wingIndex).getId().equalsIgnoreCase(getWingId())) {
//            // TODO add other wing to cargo so it is not lost (not quite sure how to do this??)
//            ship.getVariant().setWingId(this.wingIndex, getWingId());
//        }
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return (ship.getVariant().hasHullMod(getRequiredHullMod())
                && !ship.getVariant().hasHullMod(getOppositeHullMod())
                && ship.getHullSpec().getFighterBays() == 0
                && !ship.getVariant().hasHullMod(HullMods.CONVERTED_BAY)
                && isCargoSpace(ship));
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if(!ship.getVariant().hasHullMod(getRequiredHullMod())) return "Missing required hullmod Network Pattern Etching";
        if(ship.getVariant().hasHullMod(getOppositeHullMod())) return "Incompatible with another version of this hull mod";
        if (ship.getHullSpec().getFighterBays() != 0) return "Ship has existing standard fighter bay(s)";
        if (ship.getVariant().hasHullMod(HullMods.CONVERTED_BAY)) return "Ship has existing fighter bay";
        if (!isCargoSpace(ship)) return "Not enough maximum cargo space";

        return null;
    }

    @Override
    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize, ShipAPI ship) {
        if (index == 0) return "" + (int) getRequiredCargoSpace();
        return null;
    }

    private boolean isCargoSpace(ShipAPI ship) {
        // Need to exclude this hullmod from calc as it seems isApplicableToShip is called as a kind of post-success check
        MutableStat.StatMod existingDroneBayCargoStat = ship.getMutableStats().getCargoMod().getFlatBonus(getHullModId());
        float existingDroneBayCargo = existingDroneBayCargoStat != null
                ? existingDroneBayCargoStat.value
                : 0;
        float availableCargoSpace = ship.getMutableStats().getCargoMod().computeEffective(ship.getHullSpec().getCargo()) - existingDroneBayCargo;

        return availableCargoSpace >= getRequiredCargoSpace();
    }

    @Override
    public boolean affectsOPCosts() {
        return true;
    }
}
