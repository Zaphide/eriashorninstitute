package data.hullmods;

public class esi_DroneBaySmall extends esi_BaseDroneBay {

    protected String getHullModId() { return "esi_drone_bay_Small"; }
    //protected String getWingId() { return "esi_pd_drone_wing"; }
    //protected String getCleanUpHullModId() { return "esi_pd_drone_bay_cleanup"; }

    protected String getOppositeHullMod() {return "esi_drone_bay_large"; }
    protected String getRequiredHullMod() {return "esi_network_pattern_etching"; }
    protected float getRequiredCargoSpace() { return 35f; }
    protected float getNumFighterBaysToAdd() { return 1f; }
}