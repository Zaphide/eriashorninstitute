package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.ShipAPI;

import java.util.ArrayList;
import java.util.List;

// Based on example by AxleMC131

// Clean up left over 'mess' from removing a hull-mod added built-in wing

// NOTE: UNUSED
//
// This was originally intended to be used in conjunction with BaseDroneBay but discussed with Alex and modifying
// the hull spec like that will affect all new hulls generated from the spec (tested this and confirmed in the
// pre-mission setup). So, that method looks like it's a no-go for now :(

public abstract class esi_BaseDroneBayCleanup extends BaseHullMod {

    protected abstract String getHullModId();
    protected abstract String getWingId();
    protected abstract String getCleanUpHullModId();

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {

        // Tracked hull mod removed so clean up wings
        if (!ship.getVariant().hasHullMod(getHullModId())) {

            List<String> existingBuiltInWings = new ArrayList<String>(ship.getVariant().getHullSpec().getBuiltInWings());
            List<String> existingFittedWings = new ArrayList<String>(ship.getVariant().getFittedWings()); // Note: Can't use getNonBuiltInWings in AfterShipCreation

            // getFittedWings returns both fitted and built-in
            // So, as built-ins are listed first, remove as many as we have built-ins
            for(int i = 0; i < existingBuiltInWings.size(); i++){
                existingFittedWings.remove(0);
            }

            // Remove all wings from variant
            ship.getVariant().getHullSpec().getBuiltInWings().clear();
            ship.getVariant().getWings().clear();

            // Re-add wings (skip the wing we want to remove)
            int nextWingIndexForAdd = 0;
            for(String wId : existingBuiltInWings) {
                if(!wId.equalsIgnoreCase(getWingId())) {
                    ship.getVariant().setWingId(nextWingIndexForAdd, wId);
                    ship.getVariant().getHullSpec().getBuiltInWings().add(wId);
                    nextWingIndexForAdd++;
                }
            }

            for(String wId : existingFittedWings) {
                ship.getVariant().setWingId(nextWingIndexForAdd, wId);
                nextWingIndexForAdd++;
            }

            // Remove cleanup mod from ship
            ship.getVariant().removeMod(getCleanUpHullModId());
        }
    }
}
