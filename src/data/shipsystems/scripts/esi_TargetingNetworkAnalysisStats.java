package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;

import java.awt.*;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class esi_TargetingNetworkAnalysisStats extends BaseShipSystemScript {

    private static final float SHIP_BOOST_RANGE = 2000f;
    private static final String HULLMOD = "esi_network_pattern_etching";

    private static final float WEAPON_TURN_RATE_BOOST_PERCENT = 50f;
    private static final float AUTOFIRE_ACCURACY_BONUS_PERCENT = 100f;
    private static final float ENERGY_RANGE_BONUS_PERCENT = 25f;
    private static final float ENERGY_ROF_BONUS_PERCENT = 25f;

    private static final EnumSet<WeaponAPI.WeaponType> GLOW_WEAPON_TYPES = EnumSet.of(WeaponAPI.WeaponType.ENERGY);

    private List<ShipAPI> affectedShips;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {

        if(affectedShips == null)
            affectedShips = new ArrayList<ShipAPI>(); // Set List instance

        this.unapplyToAffectedShips(id); // Remove any already affected ships

        // Apply to boosting ship
        ShipAPI boostingShip = (ShipAPI)stats.getEntity();

        boostingShip.getMutableStats().getWeaponTurnRateBonus().modifyPercent(id, WEAPON_TURN_RATE_BOOST_PERCENT * effectLevel);
        boostingShip.getMutableStats().getAutofireAimAccuracy().modifyPercent(id, AUTOFIRE_ACCURACY_BONUS_PERCENT * effectLevel);
        boostingShip.getMutableStats().getEnergyWeaponRangeBonus().modifyPercent(id, ENERGY_RANGE_BONUS_PERCENT * effectLevel);
        boostingShip.getMutableStats().getEnergyRoFMult().modifyPercent(id, ENERGY_ROF_BONUS_PERCENT * effectLevel);
        boostingShip.setWeaponGlow(1, Color.YELLOW, GLOW_WEAPON_TYPES);

        this.affectedShips.add(boostingShip);

        // Apply boost to ships within range
        for(ShipAPI ship: AIUtils.getAlliesOnMap(boostingShip))
        {
            if (ship.isFighter())
                continue; // Don't care about fighters

            if(!ship.getVariant().hasHullMod(HULLMOD))
                continue; // Ship must have network pattern hullmod

            if (MathUtils.getDistance(boostingShip, ship) > SHIP_BOOST_RANGE)
                continue; // Ship must be in range

            ship.getMutableStats().getWeaponTurnRateBonus().modifyPercent(id, WEAPON_TURN_RATE_BOOST_PERCENT * effectLevel);
            ship.getMutableStats().getAutofireAimAccuracy().modifyPercent(id, AUTOFIRE_ACCURACY_BONUS_PERCENT * effectLevel);
            ship.getMutableStats().getEnergyWeaponRangeBonus().modifyPercent(id, ENERGY_RANGE_BONUS_PERCENT * effectLevel);
            ship.getMutableStats().getEnergyRoFMult().modifyPercent(id, ENERGY_ROF_BONUS_PERCENT * effectLevel);
            ship.setWeaponGlow(1, Color.YELLOW, GLOW_WEAPON_TYPES);

            this.affectedShips.add(ship);
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        this.unapplyToAffectedShips(id);
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("+" + (int) WEAPON_TURN_RATE_BOOST_PERCENT * effectLevel + "% increase to weapon turn rate within immediate area" , false);
        }
        if (index == 1) {
            return new StatusData("+" + (int) AUTOFIRE_ACCURACY_BONUS_PERCENT * effectLevel + "% increase to autofire accuracy within immediate area" , false);
        }
        if (index == 2) {
            return new StatusData("+" + (int) ENERGY_RANGE_BONUS_PERCENT * effectLevel + "% increase to energy weapon range within immediate area" , false);
        }
        if (index == 3) {
            return new StatusData("+" + (int) ENERGY_ROF_BONUS_PERCENT * effectLevel + "% increase to energy weapon RoF within immediate area" , false);
        }
        return null;
    }

    private void unapplyToAffectedShips(String id)
    {
        if(this.affectedShips == null || this.affectedShips.size() == 0)
            return;

        for (ShipAPI shipAPI : this.affectedShips)
        {
            shipAPI.getMutableStats().getWeaponTurnRateBonus().unmodify(id);
            shipAPI.getMutableStats().getAutofireAimAccuracy().unmodify(id);
            shipAPI.getMutableStats().getEnergyWeaponRangeBonus().unmodify(id);
            shipAPI.getMutableStats().getEnergyRoFMult().unmodify(id);
            shipAPI.setWeaponGlow(0, Color.YELLOW, GLOW_WEAPON_TYPES);
        }

        this.affectedShips.clear();
    }
}
