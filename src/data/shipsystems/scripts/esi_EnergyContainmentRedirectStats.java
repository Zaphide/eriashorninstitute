package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.combat.ShipAPI;

public class esi_EnergyContainmentRedirectStats extends BaseShipSystemScript {

    public static final float DAMAGE_BONUS_PERCENT = 25f;
    public static final float RANGE_BONUS_PERCENT = 100f;
    public static final float MAX_SPEED_PENALTY_PERCENT = -75f;
    public static final float TURN_SPEED_PENALTY_PERCENT = -75f;
    private static final float ENGINE_LENGTH = -0.75f;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        stats.getEnergyWeaponDamageMult().modifyPercent(id, DAMAGE_BONUS_PERCENT * effectLevel);
        stats.getEnergyWeaponRangeBonus().modifyPercent(id, RANGE_BONUS_PERCENT * effectLevel);
        stats.getMaxTurnRate().modifyPercent(id, TURN_SPEED_PENALTY_PERCENT * effectLevel);
        stats.getMaxSpeed().modifyPercent(id, MAX_SPEED_PENALTY_PERCENT * effectLevel);

        for(ShipEngineControllerAPI.ShipEngineAPI shipEngineAPI : ((ShipAPI)stats.getEntity()).getEngineController().getShipEngines())
            ((ShipAPI)stats.getEntity()).getEngineController().extendFlame(shipEngineAPI, ENGINE_LENGTH * effectLevel, ENGINE_LENGTH * effectLevel, ENGINE_LENGTH * effectLevel);
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getEnergyWeaponDamageMult().unmodify(id);
        stats.getEnergyWeaponRangeBonus().unmodify(id);
        stats.getMaxSpeed().unmodify(id);
        stats.getMaxTurnRate().unmodify(id);
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("+" + (int) DAMAGE_BONUS_PERCENT * effectLevel + "% energy weapon damage" , false);
        } else if (index == 1) {
            return new StatusData("+" + (int) RANGE_BONUS_PERCENT * effectLevel + "% energy weapon range", false);
        } else if (index == 2) {
            return new StatusData("" + (int) MAX_SPEED_PENALTY_PERCENT * effectLevel + "% max speed", false);
        } else if (index == 3) {
            return new StatusData("" + (int) TURN_SPEED_PENALTY_PERCENT * effectLevel + "% turn speed", false);
        }
        return null;
    }
}
