package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.FighterWingAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;

import java.util.ArrayList;
import java.util.List;

public class esi_FighterOverrideStats extends BaseShipSystemScript {

    private static final float FIGHTER_BOOST_PERCENT = 50f;
    private static final float FIGHTER_ENGINE_EXTEND = 1.20f;

    private List<ShipAPI> affectedFighterWings = new ArrayList<ShipAPI>();

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        ShipAPI ship = (ShipAPI)stats.getEntity();

        // Apply boost to fighters from ship
        for(FighterWingAPI wing: ship.getAllWings()) {

            for(ShipAPI wingMember: wing.getWingMembers()) {
                wingMember.getMutableStats().getMaxSpeed().modifyPercent(id, FIGHTER_BOOST_PERCENT * effectLevel);
                wingMember.getMutableStats().getAcceleration().modifyPercent(id, FIGHTER_BOOST_PERCENT * effectLevel);
                wingMember.getMutableStats().getDeceleration().modifyPercent(id, FIGHTER_BOOST_PERCENT * effectLevel);
                wingMember.getMutableStats().getMaxTurnRate().modifyPercent(id, FIGHTER_BOOST_PERCENT * effectLevel);
                wingMember.getMutableStats().getTurnAcceleration().modifyPercent(id, FIGHTER_BOOST_PERCENT * effectLevel);

                wingMember.getMutableStats().getFighterWingRange().modifyPercent(id, FIGHTER_BOOST_PERCENT * effectLevel);

                for(ShipEngineControllerAPI.ShipEngineAPI shipEngineAPI : wingMember.getEngineController().getShipEngines())
                    wingMember.getEngineController().extendFlame(shipEngineAPI, FIGHTER_ENGINE_EXTEND * effectLevel, FIGHTER_ENGINE_EXTEND * effectLevel, FIGHTER_ENGINE_EXTEND * effectLevel);

                this.affectedFighterWings.add(wingMember);
            }
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        this.unapplyToAffectedWings(id);
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("+" + (int) FIGHTER_BOOST_PERCENT * effectLevel + "% increase to fighter speed, maneuverability and range" , false);
        }
        return null;
    }

    private void unapplyToAffectedWings(String id)
    {
        if(this.affectedFighterWings == null || this.affectedFighterWings.size() == 0)
            return;

        for (ShipAPI shipAPI : this.affectedFighterWings)
        {
            shipAPI.getMutableStats().getMaxSpeed().unmodify(id);
            shipAPI.getMutableStats().getAcceleration().unmodify(id);
            shipAPI.getMutableStats().getDeceleration().unmodify(id);
            shipAPI.getMutableStats().getMaxTurnRate().unmodify(id);
            shipAPI.getMutableStats().getTurnAcceleration().unmodify(id);

            shipAPI.getMutableStats().getFighterWingRange().unmodify(id);

            for(ShipEngineControllerAPI.ShipEngineAPI shipEngineAPI : shipAPI.getEngineController().getShipEngines())
                shipAPI.getEngineController().extendFlame(shipEngineAPI, 0,  0,  0);
        }

        this.affectedFighterWings.clear();
    }
}
