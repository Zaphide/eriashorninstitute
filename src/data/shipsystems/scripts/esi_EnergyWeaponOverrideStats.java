package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;

public class esi_EnergyWeaponOverrideStats extends BaseShipSystemScript {

	public static final float ROF_BONUS = 2f;
	public static final float FLUX_REDUCTION_PERCENT = 50f;
	public static final float RANGE_BONUS_PERCENT = 50f;

	@Override
	public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
		
		float mult = 1f + ROF_BONUS * effectLevel;
		stats.getEnergyRoFMult().modifyMult(id, mult);
		stats.getEnergyWeaponFluxCostMod().modifyPercent(id, -FLUX_REDUCTION_PERCENT);
		stats.getEnergyWeaponRangeBonus().modifyPercent(id, RANGE_BONUS_PERCENT);
	}

	@Override
	public void unapply(MutableShipStatsAPI stats, String id) {
		stats.getBallisticRoFMult().unmodify(id);
		stats.getBallisticWeaponFluxCostMod().unmodify(id);
		stats.getEnergyWeaponRangeBonus().unmodify(id);
	}

	@Override
	public StatusData getStatusData(int index, State state, float effectLevel) {
		float mult = 1f + ROF_BONUS * effectLevel;
		float bonusPercent = (int) ((mult - 1f) * 100f);
		if (index == 0) {
			return new StatusData("energy weapon rate of fire +" + (int) bonusPercent + "%", false);
		}
		if (index == 1) {
			return new StatusData("energy weapon flux use -" + (int) FLUX_REDUCTION_PERCENT + "%", false);
		}
		if (index == 2) {
			return new StatusData("energy weapon range +" + (int) RANGE_BONUS_PERCENT + "%", false);
		}
		return null;
	}
}
