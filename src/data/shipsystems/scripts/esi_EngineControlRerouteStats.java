package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;

public class esi_EngineControlRerouteStats extends BaseShipSystemScript {

	public static final float ACCEL_BONUS_PERCENT = 500f;
	public static final float MAX_SPEED_BONUS = 75f;

	@Override
	public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
		if (state == ShipSystemStatsScript.State.OUT) {
			stats.getMaxSpeed().modifyFlat(id, 100f * effectLevel);
			stats.getAcceleration().modifyPercent(id, 100f * effectLevel);
			stats.getDeceleration().modifyPercent(id, 100f * effectLevel);
		} else {
			stats.getMaxSpeed().modifyFlat(id, MAX_SPEED_BONUS* effectLevel);
			stats.getAcceleration().modifyPercent(id, ACCEL_BONUS_PERCENT * effectLevel);
			stats.getDeceleration().modifyPercent(id, ACCEL_BONUS_PERCENT * effectLevel);
			//stats.getTurnAcceleration().modifyFlat(id, 50f * effectLevel);
			//stats.getTurnAcceleration().modifyPercent(id, ACCEL_BONUS_PERCENT * effectLevel);
			//stats.getMaxTurnRate().modifyFlat(id, 25f);
			//stats.getMaxTurnRate().modifyPercent(id, 150f);
		}
	}

	@Override
	public void unapply(MutableShipStatsAPI stats, String id) {
		stats.getMaxSpeed().unmodify(id);
		//stats.getMaxTurnRate().unmodify(id);
		//stats.getTurnAcceleration().unmodify(id);
		stats.getAcceleration().unmodify(id);
		stats.getDeceleration().unmodify(id);
	}

	@Override
	public StatusData getStatusData(int index, State state, float effectLevel) {
		if (index == 0) {
			return new StatusData("improved acceleration", false);
		} else if (index == 1) {
			return new StatusData("+75 top speed", false);
		}
		return null;
	}
}
