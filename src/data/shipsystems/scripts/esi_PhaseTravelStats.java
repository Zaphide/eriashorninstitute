package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

public class esi_PhaseTravelStats extends BaseShipSystemScript {

    public static final float MAX_SPEED_BONUS_PERCENT = 200f;
    public static final float ACCELERATION_BONUS_PERCENT = 200f;
    public static final float DECELERATION_BONUS_PERCENT = 200f;
    public static final float TURN_ACCELERATION_BONUS_PERCENT = 25f;
    public static final float TURN_RATE_BONUS_PERCENT = 25f;

    public static final float MAX_TIME_MULT = 2f;

    @Override
	public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {

        ShipAPI ship = null;
        boolean player = false;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
            player = ship == Global.getCombatEngine().getPlayerShip();
            id = id + "_" + ship.getId();
        } else {
            return;
        }

        // Apply speed  changes
		if (state == ShipSystemStatsScript.State.OUT) {
			stats.getMaxSpeed().modifyPercent(id, 100f * effectLevel); // to slow down ship to its regular top speed while powering drive down
            stats.getAcceleration().modifyPercent(id, 100f * effectLevel);
            stats.getDeceleration().modifyPercent(id, 100f * effectLevel);
            stats.getTurnAcceleration().modifyPercent(id, 100 * effectLevel);
            stats.getMaxTurnRate().modifyPercent(id, 100f * effectLevel);

            ship.setPhased(false);
		} else {
            stats.getMaxSpeed().modifyPercent(id, MAX_SPEED_BONUS_PERCENT * effectLevel);
            stats.getAcceleration().modifyPercent(id, ACCELERATION_BONUS_PERCENT * effectLevel);
            stats.getDeceleration().modifyPercent(id, DECELERATION_BONUS_PERCENT * effectLevel);
            stats.getTurnAcceleration().modifyPercent(id, TURN_ACCELERATION_BONUS_PERCENT * effectLevel);
            stats.getMaxTurnRate().modifyPercent(id, TURN_RATE_BONUS_PERCENT * effectLevel);

            if (effectLevel > 0.3f) {
                ship.setPhased(true);
            } else {
                ship.setPhased(false);
            }
		}

        // Apply time changes
        float shipTimeMult = 1f + (MAX_TIME_MULT - 1f) * effectLevel;
        stats.getTimeMult().modifyMult(id, shipTimeMult);
        if (player) {
            Global.getCombatEngine().getTimeMult().modifyMult(id, 1f / shipTimeMult);
        } else {
            Global.getCombatEngine().getTimeMult().unmodify(id);
        }
	}

    @Override
	public void unapply(MutableShipStatsAPI stats, String id) {

        // Remove speed changes
		stats.getMaxSpeed().unmodify(id);
        stats.getAcceleration().unmodify(id);
        stats.getDeceleration().unmodify(id);
        stats.getTurnAcceleration().unmodify(id);
        stats.getMaxTurnRate().unmodify(id);

        // Remove time changes
        ShipAPI ship = null;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
        } else {
            return;
        }

        Global.getCombatEngine().getTimeMult().unmodify(id);
        stats.getTimeMult().unmodify(id);

        ship.setPhased(false);
	}

    @Override
	public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("+" + (int) (MAX_SPEED_BONUS_PERCENT * effectLevel) + "% max speed" , false);
        } else if (index == 1) {
            return new StatusData("+" + (int) (ACCELERATION_BONUS_PERCENT * effectLevel) + "% acceleration", false);
        } else if (index == 2) {
            return new StatusData("+" + (int) (DECELERATION_BONUS_PERCENT * effectLevel) + "% deceleration", false);
        } else if (index == 3) {
            return new StatusData("+" + (int) (TURN_ACCELERATION_BONUS_PERCENT * effectLevel) + "% turn acceleration", false);
        } else if (index == 4) {
            return new StatusData("+" + (int) (TURN_RATE_BONUS_PERCENT * effectLevel) + "% turn speed", false);
        }

        return null;
	}
}
