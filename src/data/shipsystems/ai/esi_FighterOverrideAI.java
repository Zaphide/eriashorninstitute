package data.shipsystems.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class esi_FighterOverrideAI implements ShipSystemAIScript {

    //private static final float FIGHTER_BOOST_RANGE = 3000f;
    //private static final String HULLMOD = "esi_network_pattern_etching";

    private ShipSystemAPI cloak;
    private ShipAPI ship;
    private ShipSystemAPI system;

    private final IntervalUtil tracker = new IntervalUtil(0.5f, 1f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (system.getCooldownRemaining() > 0) {
                return;
            }
            if (system.isOutOfAmmo()) {
                return;
            }
            if (system.isActive()) {
                return;
            }
            if (cloak != null && (cloak.isActive() || cloak.isOn())) {
                return; // do not try to activate while cloaked
            }

            if(ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.IN_ATTACK_RUN) || ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.CARRIER_FIGHTER_TARGET)){
                for(FighterWingAPI wing: ship.getAllWings()) {
                    for (ShipAPI wingMember : wing.getWingMembers()) {
                        //if(MathUtils.getDistance(ship, wingMember) > (wing.getRange() * 0.5f)){
                        if(wingMember.getShipTarget() != null && (MathUtils.getDistance(ship, wingMember) > MathUtils.getDistance(wingMember.getShipTarget(), wingMember)
                            || MathUtils.getDistance(wingMember.getShipTarget(), wingMember) < 500)) {
                            ship.useSystem();
                            break;
                        }
                    }
                }
            }

//            for(ShipAPI wing: AIUtils.getAlliesOnMap(ship)) {
//                if (wing.isFighter() && wing.getWing() != null) {
////                    if(wing.isLanding())
////                    {
////                        ship.useSystem();
////                        break;
////                    }
//
//                    if (wing.getWing().getSourceShip().getVariant().hasHullMod(HULLMOD)
//                            && !wing.getWing().getSourceShip().isPullBackFighters()
//                            && MathUtils.getDistance(ship, wing) < FIGHTER_BOOST_RANGE)
//                    {
//                        ship.useSystem();
//                        break;
//                    }
//                }
//            }
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.system = system;
        this.cloak = ship.getPhaseCloak();
    }
}
