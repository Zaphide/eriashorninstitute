package data.shipsystems.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class esi_EmplacementDroneAI implements ShipSystemAIScript {

    private ShipSystemAPI cloak;
    private ShipAPI ship;
    private ShipSystemAPI system;

    private final IntervalUtil tracker = new IntervalUtil(0.5f, 1f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target)
    {
        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (system.getCooldownRemaining() > 0) {
                return;
            }
            if (system.isOutOfAmmo()) {
                return;
            }
            if (system.isActive()) {
                return;
            }
            if (cloak != null && (cloak.isActive() || cloak.isOn())) {
                return; // do not try to activate while cloaked
            }
            if(target == null || target.isFighter())
                return; // Don't bother to activate if ship has no target or target is fighter wing

            // Derive max range for system
            float emplacementDroneDeploymentRange = 700f; // default
            if(system.getId().equalsIgnoreCase("esi_emplacement_drones"))
                emplacementDroneDeploymentRange = 900f;
//            else if(system.getId().equalsIgnoreCase("esi_emplacement_drones_small"))
//                emplacementDroneDeploymentRange = 700f;

            // If in range, use system
            if(MathUtils.getDistance(ship, target) < emplacementDroneDeploymentRange )
            {
                ship.useSystem();
            }
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.system = system;
        this.cloak = ship.getPhaseCloak();
    }
}
