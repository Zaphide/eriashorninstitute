package data.shipsystems.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class esi_TargetingNetworkAnalysisAI implements ShipSystemAIScript {

    private static final float SHIP_BOOST_RANGE = 2000f;
    private static final float PLAYER_SHIP_FLUX_PERCENT = 0.25f;
    private static final String HULLMOD = "esi_network_pattern_etching";

    private ShipSystemAPI cloak;
    private ShipAPI ship;
    private ShipSystemAPI system;

    private final IntervalUtil tracker = new IntervalUtil(0.5f, 1f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (system.getCooldownRemaining() > 0) {
                return;
            }
            if (system.isOutOfAmmo()) {
                return;
            }
            if (system.isActive()) {
                return;
            }
            if (cloak != null && (cloak.isActive() || cloak.isOn())) {
                return; // do not try to activate while cloaked
            }

            for(ShipAPI ship: AIUtils.getAlliesOnMap(this.ship)) {
                if (ship.isFighter())
                    continue; // Don't care about fighters

                if(!ship.getVariant().hasHullMod(HULLMOD))
                    continue; // Ship must have network pattern hullmod

                if (MathUtils.getDistance(this.ship, ship) > SHIP_BOOST_RANGE)
                    continue; // Ship must be in range

                if(ship.getFluxTracker().isOverloadedOrVenting())
                    continue; // No point in boosting this ship

                if(ship.getAIFlags() != null
                        && (ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.HARASS_MOVE_IN)
                        || ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.BACK_OFF)
                        || ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.PURSUING)
                        || ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.MANEUVER_TARGET)
                        || ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.NEEDS_HELP))){
                    // AI-controlled ship <doing stuff> so use system
                    this.ship.useSystem();
                    break;
                }


                if(ship.getAIFlags() == null
                    && ship.getFluxTracker().getCurrFlux() / ship.getFluxTracker().getMaxFlux() > PLAYER_SHIP_FLUX_PERCENT)
                {
                    // Player-controlled ship that is over flux percent for activation so use system
                    this.ship.useSystem();
                    break;
                }
            }
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.system = system;
        this.cloak = ship.getPhaseCloak();
    }
}
