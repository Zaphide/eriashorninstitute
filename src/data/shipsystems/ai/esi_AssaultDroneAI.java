package data.shipsystems.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class esi_AssaultDroneAI implements ShipSystemAIScript {

    private ShipSystemAPI cloak;
    private ShipAPI ship;
    private ShipSystemAPI system;
    private JSONObject json;
    private int range;

    private final IntervalUtil tracker = new IntervalUtil(0.5f, 1f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target){

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            //Global.getLogger(this.getClass()).info("State before: " + system.getState().toString());
            //ship.useSystem();
            //ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK, null, 0);
            Global.getLogger(this.getClass()).info("ammo: " + system.getAmmo());

            if(this.getDroneOrders() == DroneLauncherShipSystemAPI.DroneOrders.DEPLOY)
                ship.useSystem(); // Don't want to jsut have drones deployed ever, so just shunt to next state

            if(target != null && MathUtils.getDistance(ship, target) < (this.range - 150)) {
                if (this.getDroneOrders() == DroneLauncherShipSystemAPI.DroneOrders.RECALL) {
                    ship.useSystem(); // Attack
                }
            } else {
                if (this.getDroneOrders() == DroneLauncherShipSystemAPI.DroneOrders.ATTACK) {
                    ship.useSystem(); // Recall
                }
            }
        }

    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.system = system;
        this.cloak = ship.getPhaseCloak();
        this.json = ((DroneLauncherShipSystemAPI)system).getSpecJson();
        this.range = this.getRange();
    }

    private DroneLauncherShipSystemAPI.DroneOrders getDroneOrders() {
        return ((DroneLauncherShipSystemAPI)system).getDroneOrders();
    }


    private int getRange() {
        try {
            return this.json.getJSONArray("droneBehavior").getJSONObject(0).getInt("freeRoamRange");
        } catch (JSONException e) {
            Global.getLogger(this.getClass()).info("Missing or malformed field in drone system JSON");
            return 1000;
        }
    }
}
