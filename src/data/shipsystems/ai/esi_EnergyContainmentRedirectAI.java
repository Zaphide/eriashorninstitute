package data.shipsystems.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.WeaponUtils;
import org.lwjgl.util.vector.Vector2f;

public class esi_EnergyContainmentRedirectAI implements ShipSystemAIScript {

    private static final float RANGE_MULTIPLER = 2f;
    private static final float RANGE_TOLERANCE_PERCENT = 0.10f;
    private static final float HARD_FLUX_LEVEL_ACTIVATION_THRESHOLD = 0.80f;

    private ShipSystemAPI cloak;
    private ShipAPI ship;
    private ShipSystemAPI system;

    // TODO could extend to support multiple weapons?
    private float maxNormalWeaponRange = 0f;
    private float systemWeaponRange = 0f;
    private WeaponAPI maxRangeWeapon = null;

    private final IntervalUtil tracker = new IntervalUtil(0.5f, 1f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (system.getCooldownRemaining() > 0) {
                return;
            }
            if (system.isOutOfAmmo()) {
                return;
            }
            if (cloak != null && (cloak.isActive() || cloak.isOn())) {
                return; // do not try to activate while cloaked
            }
            if(target == null) {
                return; // Don't bother to activate if no target
            }
            if(this.maxNormalWeaponRange == 1) {
                return; // assume weapon range 1 means no acceptable weapons found
            }

            float hard_flux_level = ship.getFluxTracker().getHardFlux() / ship.getFluxTracker().getMaxFlux();
            if (hard_flux_level >= HARD_FLUX_LEVEL_ACTIVATION_THRESHOLD) {
                if(system.isActive())
                    ship.useSystem(); // Deactivate

                return; // Don't use system if high on hard flux
            }

            if(this.maxNormalWeaponRange == 0)
            {
                // We haven'y yet calculated normal max weapon range, so do it now
                for(WeaponAPI weaponAPI : ship.getAllWeapons())
                {
                    if(weaponAPI.getType() == WeaponAPI.WeaponType.ENERGY)
                    {
                        if(weaponAPI.getRange() > this.maxNormalWeaponRange) {
                            this.maxNormalWeaponRange = weaponAPI.getRange();
                            this.maxRangeWeapon = weaponAPI;
                        }
                    }
                }

                // If normal max weapon range still 0, ship must have no energy weapons, so set as 1 to bypass
                if(this.maxNormalWeaponRange == 0)
                    this.maxNormalWeaponRange = 1;

                // Set system range
                this.systemWeaponRange = (this.maxNormalWeaponRange * RANGE_MULTIPLER) * (1 - RANGE_TOLERANCE_PERCENT);
            }

            // Check if target would be in extended range
            float targetDist = MathUtils.getDistance(ship, target);
            if(targetDist < this.systemWeaponRange && targetDist > this.maxNormalWeaponRange)
            {
                // TODO Check if ship is facing target

                if(!system.isActive())
                    ship.useSystem();
            }
            else if(system.isActive())
            {
                // Not in range and system is active, so deactivate
                ship.useSystem();
            }
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.system = system;
        this.cloak = ship.getPhaseCloak();
    }
}
