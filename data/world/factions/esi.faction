{
    "id":"esi",

    "color":[243,212,56,150],

    "baseUIColor":[243,212,56,150],
    # "darkUIColor":[5,100,200,175],
    # "gridUIColor":[50,150,225,75],

    "secondaryUIColor":[255,255,255,255],
    "secondarySegments":2,

    "displayName":"Esiran Synarch",
    "displayNameWithArticle":"the Esiran Synarch",
    "displayNameLong":"Esiran Synarch",
    "displayNameLongWithArticle":"the Esiran Synarch",
    "displayNameIsOrAre":"is",
    "personNamePrefixAOrAn":"an",

    "logo":"graphics/esi/factions/esi.png",
    "crest":"graphics/esi/factions/esi_crest.png",

    "shipNamePrefix":"ES",

    "shipNameSources":{
        "esi":1,
        "GREEK":2,
        "NORDIC":3,
        "SPACE":4
    },

    "names":{
        "modern":1,
        "world":1,
        "future":1,
        "myth":1
    },

    # variantOverrides restricts hulls to listed variants and adjusts their probability
    "variantOverrides":{
    },

    # multiplier for how often hulls show up in the faction's fleets
    "hullFrequency":{
        "tags":{

        },
        "hulls":{
            "buffalo2":0,
        },
    },

    # ships the faction gets access to when importing S&W out-of-faction
    "shipsWhenImporting":{
        "tags":["base_bp", "midline_bp", "hightech_bp"],
        "hulls":[
        ],
    },

    "knownShips":{
        "tags":["base_bp", "midline_bp", "esi_c_bp", "esi_r_bp"],
        "hulls":[
        ],
    },

    # listing ships here will make the faction mostly use them even if other hulls become available
    "priorityShips":{
        "tags":["esi_c_bp", "esi_r_bp"],
        "hulls":[
        ],
    },

    "knownFighters":{
        "tags":["midline_bp", "hightech_bp", "esi_c_bp", "esi_r_bp"],
        "fighters":[
        ],
    },

    "priorityFighters":{
        "tags":["esi_c_bp", "esi_r_bp"],
        "fighters":[
        ],
    },

    "knownWeapons":{
        "tags":["base_bp", "midline_bp", "hightech_bp", "missile_bp", "esi_c_bp", "esi_r_bp"],
        "weapons":[
        ],
    },

    "priorityWeapons":{
        "tags":["esi_c_bp", "esi_r_bp"],
        "weapons":[
        ],
    },

    "knownHullMods":{
        "tags":["base_bp", "midline_bp", "hightech_bp", "esi_c_bp", "esi_r_bp"],
        "hullMods":[
        ],
    },

    "factionDoctrine":{
        "warships":4,
        "carriers":2,
        "phaseShips":1,

        "officerQuality":3,
        "shipQuality":2,
        "numShips":3,

        "shipSize":3,

        "aggression":2,

        "combatFreighterProbability":0.5,                       # instead of some portion of the freighters in a fleet
        "combatFreighterCombatUseFraction":0.4,                 # as part of the normal combat lineup
        "combatFreighterCombatUseFractionWhenPriority":0.4,     # as part of normal combat lineup, when marked as priority ship
        "autofitRandomizeProbability":0.3,

        "commanderSkillsShuffleProbability":0,
        "commanderSkills":[
            "electronic_warfare",
            "coordinated_maneuvers",
            "officer_management",
            "fighter_doctrine",
            "sensors", #better sensor range
            "navigation", #better speed
        ],
    },


    "illegalCommodities":[
        "hand_weapons",
        "organs",
    ],

    "internalComms":"esi_internal",

    "music":{
        "theme":"music_diktat_market_neutral",
        "market_neutral":"music_diktat_market_neutral",
        "market_hostile":"music_diktat_market_hostile",
        "market_friendly":"music_diktat_market_friendly",
        "encounter_neutral":"music_diktat_encounter_neutral",
        "encounter_hostile":"music_diktat_encounter_hostile",
        "encounter_friendly":"music_diktat_encounter_friendly",
    },

    "portraits":{
        "standard_male":[
            "graphics/portraits/portrait_corporate01.png",
            "graphics/portraits/portrait_corporate03.png",
            "graphics/portraits/portrait_corporate05.png",
            "graphics/portraits/portrait_corporate06.png",
            "graphics/portraits/portrait13.png",
        ],
        "standard_female":[
            "graphics/portraits/portrait_corporate02.png",
            "graphics/portraits/portrait_corporate04.png",
            "graphics/portraits/portrait_mercenary02.png",
            "graphics/portraits/portrait16.png",
            "graphics/portraits/portrait_corporate04.png",
            "graphics/portraits/portrait_corporate07.png",
        ],
    },

    "ranks":{
        "ranks":{
            "spaceCommander":{"name":"Executor"},
        },
        "posts":{
            "patrolCommander":{"name":"Tactician"},
            "fleetCommander":{"name":"Marshal"},
            "baseCommander":{"name":"Steward"},
        },
    },
    
    "custom":{
		"allowsTransponderOffTrade":false,
        "offersCommissions":true,
        "engagesInHostilities":true,
        "buysAICores":true,
        "AICoreValueMult":3,
        "AICoreRepMult":1,
        "buysSurveyData":true,
        "hostilityImpactOnGrowth":true,
        "punitiveExpeditionData":{
            "vsCompetitors":true,
            "vsFreePort":false,
            "canBombard":true,
            "territorial":true,
        },
    },

    "fleetTypeNames":{
		"trade":"Trader",
		"tradeLiner":"Convoy",
		"tradeSmuggler":"Courier",
        "smallTrader":"Associate Trader",

		"patrolSmall":"Observers",
		"patrolMedium":"Vigil Patrol",
		"patrolLarge":"Sentinel Fleet",

		"inspectionFleet":"Assessor Fleet",
        "taskForce":"Company",

        "foodReliefFleet":"Relief Company",

        "mercScout":"Scout",
        "mercBountyHunter":"Hunter",
        "mercPrivateer":"Raiders",
        "mercPatrol":"Attack Fleet",
        "mercArmada":"Armada",
    }
}